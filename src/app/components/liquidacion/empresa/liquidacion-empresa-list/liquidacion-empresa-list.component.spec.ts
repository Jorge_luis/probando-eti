import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidacionEmpresaListComponent } from './liquidacion-empresa-list.component';

describe('LiquidacionEmpresaListComponent', () => {
  let component: LiquidacionEmpresaListComponent;
  let fixture: ComponentFixture<LiquidacionEmpresaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidacionEmpresaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidacionEmpresaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
