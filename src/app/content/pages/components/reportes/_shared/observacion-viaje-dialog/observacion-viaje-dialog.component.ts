import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'm-observacion-viaje-dialog',
  templateUrl: './observacion-viaje-dialog.component.html',
  styleUrls: ['./observacion-viaje-dialog.component.scss']
})
export class ObservacionViajeDialogComponent implements OnInit {
  viewLoading: boolean = false;
  usuario: string;
  observacion = new FormControl('', [Validators.required]);

  constructor(
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
		public dialogRef: MatDialogRef<ObservacionViajeDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
    console.log(this.usuario);
  }

  onNoClick(): void {
		this.dialogRef.close(['', true]);
  }

  onYesClick(): void {
    if (this.observacion.status === 'INVALID') {
      this.observacion.markAsTouched();
      return;
    }
		/* Server loading imitation. Remove this */
		this.viewLoading = true;
    this.liquidacion.putObservacionViaje(this.data.viaje, this.observacion.value, this.usuario).subscribe((data: any) => {
      console.log(data);
      if (data > 0) {
        this.dialogRef.close([data, true]);
        this.toastr.successToastr('Se registro la observación correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al registrar la observación.', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      this.toastr.errorToastr('Se produjo un error al registrar la observación.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

}
