import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'm-aprobar-liquidacion-dialog',
  templateUrl: './aprobar-liquidacion-dialog.component.html',
  styleUrls: ['./aprobar-liquidacion-dialog.component.scss']
})
export class AprobarLiquidacionDialogComponent implements OnInit {
  viewLoading: boolean = false;
  usuario: string;

  constructor(
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
    public dialogRef: MatDialogRef<AprobarLiquidacionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
  }


  onNoClick(): void {
		this.dialogRef.close(['', true]);
  }

  onYesClick(): void {
		/* Server loading imitation. Remove this */
    this.viewLoading = true;
    this.liquidacion.putAprobarLiquidacion(this.data.liquidacion, this.data.estado, this.usuario).subscribe((data: any) => {
      console.log(data);
      if (data > 0) {
        this.dialogRef.close([data, true]);
        if (this.data.estado === '0002') {
          this.toastr.successToastr('Se aprobó liquidación correctamente.', 'Exito!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          this.toastr.successToastr('Se rechazó la liquidación correctamente.', 'Exito!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        }
      } else {
        this.dialogRef.close(true);
        if (this.data.estado === '0002') {
          this.toastr.errorToastr('Se produjo un error al aprobar la liquidación..', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          this.toastr.errorToastr('Se produjo un error al rechazar la liquidación..', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        }
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      if (this.data.estado === '0002') {
        this.toastr.errorToastr('Se produjo un error al aprobar la liquidación..', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.toastr.errorToastr('Se produjo un error al rechazar la liquidación..', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
      console.log(errorServicio);
    });
  }
}
