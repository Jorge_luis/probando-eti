import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReportesComponent } from '././reportes.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CodePreviewModule } from '../../../partials/content/general/code-preview/code-preview.module';
import { PartialsModule } from '../../../partials/partials.module';
import { CoreModule } from '../../../../core/core.module';
import { MaterialPreviewModule } from '../../../partials/content/general/material-preview/material-preivew.module';
import { NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//import { NgbdModalContentComponent } from '../ngbootstrap/./modal/modal.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { HistorialAprobacionCorporativosComponent } from './historial-aprobacion-corporativos/historial-aprobacion-corporativos.component';
import { DateFormat } from '../procesos/liquidacion/date-format';


import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule,
    DateAdapter
} from '@angular/material';
import { ServiciosCorporativosComponent } from './servicios/servicios-corporativos/servicios-corporativos.component';
import { ServiciosGeneralComponent } from './servicios/servicios-general/servicios-general.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LiquidacionListComponent } from './liquidaciones/liquidacion-list/liquidacion-list.component';
import { LiquidacionDetailsComponent } from './liquidaciones/liquidacion-details/liquidacion-details.component';
import { SerciosALiquidarComponent } from './servicios/sercios-aliquidar/sercios-aliquidar.component';
import { UtilsService } from './_core/utils/utils.service';
import { ObservacionViajeDialogComponent } from './_shared/observacion-viaje-dialog/observacion-viaje-dialog.component';
import { AprobarLiquidacionDialogComponent } from './_shared/aprobar-liquidacion-dialog/aprobar-liquidacion-dialog.component';

const routes: Routes = [
	{
		path: '',
		component: ReportesComponent,
		children: [
			{
				path: 'servicios-corporativos',
				component: ServiciosCorporativosComponent
			},
			{
				path: 'servicios-general',
				component: ServiciosGeneralComponent
			},
			{
				path: 'historialServicios',
				component: HistorialAprobacionCorporativosComponent
			},
			{
				path: 'liquidaciones',
				component: LiquidacionListComponent
			},
			{
				path: 'liquidaciones/details',
				component: LiquidacionDetailsComponent
			},
			{
				path: 'liquidaciones/details/:id',
				component: LiquidacionDetailsComponent
			},
			{
				path: 'servicios-a-liquidar',
				component: SerciosALiquidarComponent
			},
		]
	}
];

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		NgbModule,
		CodePreviewModule,
		CoreModule,
		MaterialPreviewModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		PerfectScrollbarModule,
		MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
		MatProgressSpinnerModule,
		MatIconModule,
		MatSelectModule,
		MatMenuModule,
		MatProgressBarModule,
		MatButtonModule,
		MatCheckboxModule,
		MatDialogModule,
		MatTabsModule,
		MatNativeDateModule,
		MatCardModule,
		MatRadioModule,
		MatDatepickerModule,
		MatAutocompleteModule,
		MatSnackBarModule,
		MatTooltipModule,
		AngularFontAwesomeModule
	],
	exports: [RouterModule],
	declarations: [
        ReportesComponent,
        HistorialAprobacionCorporativosComponent,
        ServiciosCorporativosComponent,
        ServiciosGeneralComponent,
        LiquidacionListComponent,
        LiquidacionDetailsComponent,
        SerciosALiquidarComponent,
        ObservacionViajeDialogComponent,
        AprobarLiquidacionDialogComponent
	],
	providers: [
		NgbAlertConfig, {
		provide: PERFECT_SCROLLBAR_CONFIG,
		useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
		useClass: DateFormat
	  },
	  UtilsService
	],
	entryComponents: [
		ObservacionViajeDialogComponent,
		AprobarLiquidacionDialogComponent
	]
})
export class ReportesModule {
	constructor(private dateAdapter: DateAdapter<Date>) {
		dateAdapter.setLocale('en-in'); // DD/MM/YYYY
	  }
}
