import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ObservacionViajeDialogComponent } from '../../_shared/observacion-viaje-dialog/observacion-viaje-dialog.component';
import { AprobarLiquidacionDialogComponent } from '../../_shared/aprobar-liquidacion-dialog/aprobar-liquidacion-dialog.component';

export enum MessageType {
	Create,
	Read,
	Update,
	Delete
}

@Injectable()
export class UtilsService {
  constructor(
    private dialog: MatDialog
  ) {}

  	/* METODOS DE APROBACION DE USUARIOS */
		addObservacion(title: string = '', description: string = '', waitDesciption: string = '', viaje: number) {
		return this.dialog.open(ObservacionViajeDialogComponent, {
			data: { title, description, waitDesciption,  viaje},
			width: '440px'
		});
	}

	aprobarLiquidacion(title: string = '', description: string = '', waitDesciption: string = '', liquidacion: number, estado: string) {
		return this.dialog.open(AprobarLiquidacionDialogComponent, {
			data: { title, description, waitDesciption,  liquidacion, estado},
			width: '440px'
		});
	}
}
