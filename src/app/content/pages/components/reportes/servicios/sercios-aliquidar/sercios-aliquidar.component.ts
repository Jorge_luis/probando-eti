import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ReportesService } from '../../../../../../core/services/reportes.service';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import * as ExcelProper from 'ExcelJS';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'm-sercios-aliquidar',
  templateUrl: './sercios-aliquidar.component.html',
  styleUrls: ['./sercios-aliquidar.component.scss'],
  providers: [DatePipe]
})
export class SerciosALiquidarComponent implements OnInit {
  empresa: string;
  arrayAreas: any[] = [];
  area = new FormControl();
  filteredOptions: Observable<string[]>;
  array: any[] = [];
  dataReporte: any[] = [];
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['solicitante', 'fechasolicitud', 'horasolicitud', 'vehiculo', 'origen', 'destino', 'estado', 'tarifa'];
  searchBan: boolean = false;
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  filtroArea: string;
  searchKey: string;
  sName: string;
  excelFileName: string;
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  cols = ['Fecha', 'Solicitante', 'Móvil', 'Placa', 'Origen', 'Destino', 'Tarifa (S/)', 'Estado'];
  total_view: number;

  constructor(
    private liquidacion: LiquidacionService,
    private datePipe: DatePipe,
    public toastr: ToastrManager,
    public reportes: ReportesService
  ) { }

  ngOnInit() {
    this.empresa = localStorage.getItem('cliente');
    this.getAreas();
    this.listData = new MatTableDataSource(this.array);
    this.listData.sort = this.MatSort;
    this.listData.paginator = this.paginator;
    this.searchReporteServicios();
  }

  getAreas() {
    this.liquidacion.getAreas(Number(this.empresa), 1).subscribe(
      (data: any) => {
          this.arrayAreas = data;
          this.filteredOptions = this.area.valueChanges
            .pipe(
              startWith(''),
              map(value => this._filter(value))
            );
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
  }

  _filter(value: string): string[] {
    const filterValueArea = value.toLowerCase();
    return this.arrayAreas.filter(arrayArea => (arrayArea.area).toLowerCase().includes(filterValueArea));
  }

  getServicioCssClassByEstado(status: string = ''): string {
		switch (status) {
      case '0001':
        return 'warning';
      case '0002':
        return 'default';
      case '0003':
        return 'primary';
      case '0004':
        return 'primary';
      case '0005':
				return 'warning';
      case '0006':
        return 'success';
      case '0007':
				return 'danger';
		}
		return '';
  }

  getServicioEstadoString(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'A. PENDIENTE';
      case '0002':
        return 'SOLICITADO';
      case '0003':
        return 'EN CAMINO';
      case '0004':
        return 'EN EL PUNTO';
      case '0005':
				return 'EN VIAJE';
      case '0006':
        return 'FINALIZADO';
      case '0007':
				return 'CANCELADO';
		}
		return '';
  }

  loadLiquidacionFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadLiquidacionFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }

  searchReporteServicios() {
    if (this.area.value === null || this.area.value === '') {
      this.filtroArea = null;
    } else {
      const areaId = this.arrayAreas.find(area => area.area === this.area.value);
      if (areaId !== undefined) {
        this.filtroArea = areaId['areaId'];
      } else {
        this.array = [];
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.toastr.warningToastr('El conductor ingresado no es el correcto.', 'Alerta!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        return;
      }
    }

    this.searchBan = true;
    this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
    this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));

    this.reportes.getReporteServiciosSinLiquidar(this.filtroArea, this.filtroFechaInicio, this.filtroFechaFin).subscribe(
      (data: any) => {
        this.array = data;
        this.dataReporte = data;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        this.total_view = this.array.map(t => t.tarifa).reduce((acc, value) => acc + value, 0);
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Ocurrio un problema al listar los servicios, por favor intente nuevamente.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  filterServicios() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  downloadEXCEL() {
    if (this.dataReporte.length > 0) {
      this.sName = `Servicios Sin Liquidar`;
      this.excelFileName = `Reporte_Servicios_NoLiquidados.xlsx`;
      const workbook = new Excel.Workbook();
      workbook.creator = 'Web';
      workbook.lastModifiedBy = 'Web';
      workbook.created = new Date();
      workbook.modified = new Date();
      workbook.addWorksheet(this.sName, { views: [{ state: 'frozen', ySplit: 0, xSplit: 20, activeCell: 'A1', showGridLines: true }] });
      const sheet = workbook.getWorksheet(1);
      sheet.getColumn(1).width = 30;
      sheet.getRow(1).values = this.cols;

      sheet.columns = [
        { key: 'fecha', width: 12 },
        { key: 'solicitante', width: 28 },
        { key: 'unidad' },
        { key: 'vehiculo' },
        { key: 'origen', width: 28 },
        { key: 'destino', width: 28 },
        { key: 'tarifa' },
        { key: 'destado' }
      ];

      ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1'].map(key => {
        sheet.getCell(key).fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'EDFC00' }
        };
        });

      sheet.addRows(this.dataReporte);
      workbook.xlsx.writeBuffer().then(dataReporte => {
        const blob = new Blob([dataReporte], { type: this.blobType });
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.href = url;
        a.download = this.excelFileName;
        a.click();
      });
    } else {
      this.toastr.warningToastr('No se encontraron registros a exportar.', 'Warning!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
    }
  }

}


