import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EmpresasService } from '../../../../../../core/services/empresas.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { DatePipe } from '@angular/common';
import { ToastrManager } from 'ng6-toastr-notifications';
import { ReportesService } from '../../../../../../core/services/reportes.service';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import * as ExcelProper from 'ExcelJS';
import * as FileSaver from 'file-saver';
@Component({
  selector: 'm-servicios-general',
  templateUrl: './servicios-general.component.html',
  styleUrls: ['./servicios-general.component.scss'],
  providers: [DatePipe]
})
export class ServiciosGeneralComponent implements OnInit {
  arrayConductores: any[] = [];
  conductor = new FormControl();
  unidad = new FormControl('', [Validators.pattern(/^[0-9]+$/)]);
  placa = new FormControl();
  filteredOptions: Observable<string[]>;
  array: any[] = [];
  dataReporte: any[] = [];
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['solicitante', 'fechasolicitud', 'horasolicitud', 'conductor', 'vehiculo', 'origen', 'destino', 'estado', 'tarifa'];
  searchBan: boolean = false;
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  filtroConductor: number;
  searchKey: string;
  sName: string;
  excelFileName: string;
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  cols = ['Fecha', 'H. Inicio Servicio', 'H. Fin Servicio', 'Solicitante', 'Conductor', 'Móvil', 'Placa', 'Origen', 'Destino', 'Tarifa (S/)', 'Estado'];

  constructor(
    private liquidacion: LiquidacionService,
    private datePipe: DatePipe,
    public toastr: ToastrManager,
    public reportes: ReportesService
  ) { }

  ngOnInit() {
    this.getConductores();
    this.listData = new MatTableDataSource(this.array);
    this.listData.sort = this.MatSort;
    this.listData.paginator = this.paginator;
  }

  getConductores() {
    this.liquidacion.getConductores('').subscribe(
      (data: any) => {
          this.arrayConductores = data;
          this.filteredOptions = this.conductor.valueChanges
            .pipe(
              startWith(''),
              map(value => this._filter(value))
            );
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
  }

  _filter(value: string): string[] {
    const filterValueConductor = value.toLowerCase();
    return this.arrayConductores.filter(arrayConductor => (arrayConductor.nombres).toLowerCase().includes(filterValueConductor));
  }

  getServicioCssClassByEstado(status: string = ''): string {
		switch (status) {
      case '0001':
        return 'warning';
      case '0002':
        return 'default';
      case '0003':
        return 'primary';
      case '0004':
        return 'primary';
      case '0005':
				return 'warning';
      case '0006':
        return 'success';
      case '0007':
				return 'danger';
		}
		return '';
  }

  getServicioEstadoString(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'A. PENDIENTE';
      case '0002':
        return 'SOLICITADO';
      case '0003':
        return 'EN CAMINO';
      case '0004':
        return 'EN EL PUNTO';
      case '0005':
				return 'EN VIAJE';
      case '0006':
        return 'FINALIZADO';
      case '0007':
				return 'CANCELADO';
		}
		return '';
  }

  loadLiquidacionFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadLiquidacionFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }

  searchReporteServicios() {
    if (this.conductor.value === null || this.conductor.value === '') {
      this.filtroConductor = null;
    } else {
      const conductorId = this.arrayConductores.find(conduc => conduc.nombres === this.conductor.value);
      if (conductorId !== undefined) {
        this.filtroConductor = conductorId['conductor'];
      } else {
        this.array = [];
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.toastr.warningToastr('El conductor ingresado no es el correcto.', 'Alerta!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        return;
      }
    }

    this.searchBan = true;
    this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
    this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));

    this.reportes.getReporteServicios(1, null, this.filtroConductor, this.unidad.value, (this.placa.value === null ? '' : this.placa.value), this.filtroFechaInicio, this.filtroFechaFin).subscribe(
      (data: any) => {
        this.array = data;
        this.dataReporte = data;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Ocurrio un problema al listar los servicios, por favor intente nuevamente.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  filterServicios() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  downloadEXCEL() {
    if (this.dataReporte.length > 0) {
      this.sName = `Servicios No Corporativos`;
      this.excelFileName = `Reporte_Servicios_NoCorporativos.xlsx`;
      const workbook = new Excel.Workbook();
      workbook.creator = 'Web';
      workbook.lastModifiedBy = 'Web';
      workbook.created = new Date();
      workbook.modified = new Date();
      workbook.addWorksheet(this.sName, { views: [{ state: 'frozen', ySplit: 0, xSplit: 20, activeCell: 'A1', showGridLines: true }] });
      const sheet = workbook.getWorksheet(1);
      sheet.getColumn(1).width = 30;
      sheet.getRow(1).values = this.cols;
  
      sheet.columns = [
        { key: 'fecha', width: 12 },
        { key: '', width: 12 },
        { key: '', width: 12 },
        { key: 'solicitante', width: 28 },
        { key: 'conductor', width: 28 },
        { key: 'unidad' },
        { key: 'vehiculo' },
        { key: 'origen', width: 28 },
        { key: 'destino', width: 28 },
        { key: 'tarifa' },
        { key: 'destado' }
      ];

      ['A1', 'B1', 'C1', 'D1', 'E1', 'F1', 'G1', 'H1', 'I1', 'J1', 'K1'].map(key => {
        sheet.getCell(key).fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: 'EDFC00' }
        };
        });

      sheet.addRows(this.dataReporte);
      workbook.xlsx.writeBuffer().then(dataReporte => {
        const blob = new Blob([dataReporte], { type: this.blobType });
        const url = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.href = url;
        a.download = this.excelFileName;
        a.click();
      });
    } else {
      this.toastr.warningToastr('No se encontraron registros a exportar.', 'Warning!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
    }
    
  }

}
