import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../core/services/aprobacion-solicitud.service';


@Component({
  selector: 'm-historial-aprobacion-corporativos',
  templateUrl: './historial-aprobacion-corporativos.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class HistorialAprobacionCorporativosComponent implements OnInit {

  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['dni', 'solicitante', 'fechasolicitud', 'fecharespuesta', 'origen', 'destino', 'estado'];
  filterFechaIni: Date = new Date(new Date().getTime() - (24 * 60 * 60 * 1000) * 7);
  filterFechaFin: Date = new Date();
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date(new Date().getTime() - (24 * 60 * 60 * 1000) * 7);
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  searchBan: boolean = false;
  array: any[] = [];
  searchKey: string;
  aprobador: string;

  constructor(
    private subheaderService: SubheaderService,
    private datePipe: DatePipe,
    public toastr: ToastrManager,
    private reporte: AprobacionSolicitudService
  ) { }

  ngOnInit() {
    this.subheaderService.setTitle('Historial de Servicios Aprobados');
    this.aprobador = localStorage.getItem('aprobador');
    this.searchHistorialServicios();
  }

  loadSolicitudFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadSolicitudFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }

  filterServicio() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  getSolicitudCssClassByEstado(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'warning';
      case '0002':
				return 'success';
			case '0003':
				return 'danger';
		}
		return '';
  }

  getSolicitudEstadoString(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'Pendiente';
      case '0002':
				return 'Aprobado';
			case '0003':
				return 'Rechazado';
		}
		return '';
  }

  searchHistorialServicios() {
    this.searchBan = true;
    this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
    this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));
    this.reporte.getHistorialAprobacion(this.aprobador, this.filtroFechaInicio, this.filtroFechaFin, '0001').subscribe(
      (data: any) => {
        this.array = data;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Ocurrio un error al cargar serivicios', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
        });
  }

}
