import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, Pipe, PipeTransform} from '@angular/core';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { UtilsService } from '../../_core/utils/utils.service';



@Component({
  selector: 'm-liquidacion-list',
  templateUrl: './liquidacion-list.component.html',
  styleUrls: ['./liquidacion-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class LiquidacionListComponent implements OnInit {
  empresa: string;
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  searchBan: boolean = false;
  searchKey: string;
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['fechaliquidacion', 'codigoliquidacion', 'dtipodocumento', 'documento', 'total', 'estado', 'actions'];
  filtroFechaInicio: string;
  filtroFechaFin: string;

  constructor(
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private subheaderService: SubheaderService,
    private utilsService: UtilsService,
  ) { }

  ngOnInit() {
    this.subheaderService.setTitle('Listado de Liquidaciones');
    this.empresa = localStorage.getItem('cliente');
    this.listData = new MatTableDataSource(this.array);
    this.listData.sort = this.MatSort;
    this.listData.paginator = this.paginator;
    this.searchLiquidacion();
  }

  loadLiquidacionFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadLiquidacionFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }

  searchLiquidacion() {
    this.searchBan = true;
    this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
    this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));
    this.liquidacion.getLiquidacion('0001', Number(this.empresa), this.filtroFechaInicio, this.filtroFechaFin).subscribe(
      (data: any) => {
        this.array = data;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        }, ( errorServicio ) => {
          this.array = [];
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
          console.log(errorServicio);
          this.toastr.errorToastr('Se produjo un error al listar las liquidaciones, por favor refresque la pantalla.', 'Error!', {
            toastTimeout: 3000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  getLiquidacionCssClassByEstado(status: string = '', observacion: number): string {
		switch (status) {
      case '0001':
        switch (observacion) {
          case 0:
            return 'warning';
          default:
            return 'danger';
        }
      case '0002':
        return 'primary';
      case '0003':
				return 'success';
			case '0004':
				return 'danger';
		}
		return '';
  }

  getLiquidacionEstadoString(status: string = '', observacion: number): string {
		switch (status) {
      case '0001':
        switch (observacion) {
          case 0:
            return 'Pendiente';
          default:
            return 'Con Observaciones';
        }
      case '0002':
        return 'Aprobado';
      case '0003':
				return 'Pagado';
			case '0004':
				return 'Rechazado';
		}
		return '';
  }

  filterLiquidaciones() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  aprobarLiquidacion(Liquidacion: number) {
    console.log(Liquidacion);
    const _title: string = 'Aprobar Liquidación';
		const _description: string = '¿Estas seguro de aprobar la liquidación seleccionada?';
    const _waitDesciption: string = 'Aprobando Liquidación....';
    const dialogRef = this.utilsService.aprobarLiquidacion(_title, _description, _waitDesciption, Liquidacion, '0002');
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
            this.searchLiquidacion();
        }
      });
  }
 
  rechazarLiquidacion(Liquidacion: number) {
    console.log(Liquidacion);
    const _title: string = 'Rechazar Liquidación';
		const _description: string = '¿Estas seguro de rechazar la liquidación seleccionada?';
    const _waitDesciption: string = 'Rechazando Liquidación....';
    const dialogRef = this.utilsService.aprobarLiquidacion(_title, _description, _waitDesciption, Liquidacion, '0004');
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
            this.searchLiquidacion();
        }
      });
  }

}
