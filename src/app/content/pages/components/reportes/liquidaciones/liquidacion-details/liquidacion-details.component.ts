import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy, Injectable } from '@angular/core';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { EmpresasService } from '../../../../../../core/services/empresas.service';
import { startWith, map } from 'rxjs/operators';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import * as ExcelProper from 'ExcelJS';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';
import { UtilsService } from '../../_core/utils/utils.service';


@Component({
  selector: 'm-liquidacion-details',
  templateUrl: './liquidacion-details.component.html',
  styleUrls: ['./liquidacion-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class LiquidacionDetailsComponent implements OnInit {
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  searchBan: boolean = false;
  banNuevo: boolean = true;
  update: boolean = true;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['fechasolicitud', 'fechainicioviaje', 'solicitante', 'origen', 'destino',  'unidad',  'placa', 'observacion', 'montoliquidado', 'atencionobservacion', 'actions'];
  serviciosResult: any[] = [];
  serviciosCont: any[] = [];
  nroLiquidacion: string;
  isValid: boolean = false;
  isReset: boolean = true;
  nroLiquidacion_view: string;
  fecha_view: string;
  empresa_view: string;
  total_view: string;
  liquidacion_id: number;
  dataReporte: any[] = [];
  isDetails: boolean = false;
  name: string;
  sName: string;
  excelFileName: string;
  codEmpresa: number;
  usuario: string;

  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  cols = ['Fecha', 'H. Inicio Servicio', 'H. Fin Servicio', 'Solicitante', 'Area', 'Ce.Co', 'Origen', 'Destino', 'Móvil', 'Placa', 'Conductor', 'Tarifa (S/)'];

  constructor(
    private empresa: EmpresasService,
    private router: Router,
    private subheaderService: SubheaderService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private liquidacion: LiquidacionService,
    private activatedRoute: ActivatedRoute,
    private utilsService: UtilsService
  ) {  }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
    this.activatedRoute.queryParams.subscribe(params => {
      const id = +params.id;
      this.liquidacion_id = id;
			if (id && id > 0) {
        this.subheaderService.setTitle('Detalles de Liquidación');
        this.banNuevo = false;
				this.initLiquidacion(id);
			} else {
        this.liquidacion.getCodigoLiquidacion('0001').subscribe(
          (data: any) => {
              this.nroLiquidacion = data[0]['codigo'];
            }, ( errorServicio ) => {
              console.log(errorServicio);
            });

        this.isDetails = true;
        this.initLiquidacion(id);
        this.subheaderService.setTitle('Nueva Liquidación');
        this.array = [];
        this.listData = new MatTableDataSource(this.array);
			}
    });
  }

  initLiquidacion(id: number) {
    if (id > 0) {
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Reportes', page: '/reportes/liquidaciones' },
        { title: 'Liquidaciones',  page: '/reportes/liquidaciones' },
      ]);

      this.liquidacion.getDatosLiquidacion(id).subscribe(
        (data: any) => {
          this.nroLiquidacion_view = data[0]['codigoLiquidacion'];
          this.empresa_view = data[0]['razonSocial'];
          this.total_view = data[0]['total'];
          this.fecha_view = data[0]['fechaLiquidacion'];
          this.codEmpresa = data[0]['empresa'];
          if (data[0]['estado'] === '0002' || data[0]['estado'] === '0003' || data[0]['estado'] === '0004') {
            this.isDetails = false;
            this.update = true;
          } else {
            this.isDetails = true;
            this.update = false;
          }
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
        this.searchBan = true;
        this.liquidacion.getDetalleLiquidacion(id).subscribe(
          (data: any) => {
            this.dataReporte = data;
            this.listData = new MatTableDataSource(data);
            this.listData.sort = this.MatSort;
            this.listData.paginator = this.paginator;
            this.searchBan = false;
          }, ( errorServicio ) => {
            console.log(errorServicio);
          });


      return;
    } else {
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Procesos', page: '/procesos/liquidacion-empresa' },
        { title: 'Liqu. Empresas',  page: '/procesos/liquidacion-empresa' },
        { title: 'Nueva Liquidación', page: '/procesos/liquidacion-empresa/add' }
      ]);
      return;
    }
  }

  getDetalleCssClassByEstado(status: boolean): string {
		switch (status) {
			case true:
        return 'primary';
      case false:
        return 'warning';
		}
		return 'default';
  }

  getDetalleEstadoString(status: boolean): string {
		switch (status) {
			case true:
        return 'Atendido';
      case false:
        return 'Pendiente';
		}
		return 'Sin Observaciones';
  }

  getComponentTitle() {
		let result = 'Nueva Liquidación';
		if (!this.liquidacion_id) {
			return result;
		}

		result = `Detalle de Liquidación`;
		return result;
  }

  goBack(id = 0) {
		let _backUrl = 'reportes/liquidaciones';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
  }

  downloadEXCEL() {
    console.log(this.dataReporte);
    this.sName = `${ this.nroLiquidacion_view }`;
    this.excelFileName = `Liquidacion-${ this.nroLiquidacion_view }.xlsx`;
    const workbook = new Excel.Workbook();
    workbook.creator = 'Web';
    workbook.lastModifiedBy = 'Web';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.addWorksheet(this.sName, { views: [{ state: 'frozen', ySplit: 0, xSplit: 20, activeCell: 'A1', showGridLines: true }] });
    const sheet = workbook.getWorksheet(1);
    sheet.addRow([`N° lIQUIDACION:`, `${ this.nroLiquidacion_view }`, '', `Conductor:`, `${ this.empresa_view }`]);
    sheet.addRow([`Fecha Liquidación:`, `${ this.datePipe.transform(this.fecha_view, 'dd-MM-yyyy') }`, '', `Monto Total:`, `${ this.total_view }`]);
    sheet.getColumn(4).width = 30;
    sheet.getRow(4).values = this.cols;

    sheet.columns = [
      { key: 'fecha', width: 12 },
      { key: 'horainicio', width: 12 },
      { key: 'horafin', width: 12 },
      { key: 'solicitante', width: 30 },
      { key: 'area', width: 28 },
      { key: 'centrocostos', width: 20 },
      { key: 'origen', width: 30 },
      { key: 'destino', width: 30 },
      { key: 'unidad' },
      { key: 'placa' },
      { key: 'conductor', width: 30 },
      { key: 'monto' }
    ];

    ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4', 'J4', 'K4', 'L4', 'M4'].map(key => {
      sheet.getCell(key).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'EDFC00' }
      };
      });

    
    sheet.addRows(this.dataReporte);
    workbook.xlsx.writeBuffer().then(dataReporte => {
      const blob = new Blob([dataReporte], { type: this.blobType });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.href = url;
      a.download = this.excelFileName;
      a.click();
    });
  }

  registrarObservacion(Viaje: number) {
    const _title: string = 'Registrar Observación';
		const _description: string = '¿Estas seguro de realizar una observación al viaje seleccionado?';
    const _waitDesciption: string = 'Registrando Observación....';
    const dialogRef = this.utilsService.addObservacion(_title, _description, _waitDesciption, Viaje);
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          this.liquidacion.getDetalleLiquidacion(this.liquidacion_id).subscribe(
            (data: any) => {
              this.dataReporte = data;
              this.listData = new MatTableDataSource(data);
              this.listData.sort = this.MatSort;
              this.listData.paginator = this.paginator;
              this.searchBan = false;
            }, ( errorServicio ) => {
              console.log(errorServicio);
            });
        }
      });
  }

  aprobarLiquidacion() {
    const _title: string = 'Aprobar Liquidación';
		const _description: string = '¿Estas seguro de aprobar la liquidación seleccionada?';
    const _waitDesciption: string = 'Aprobando Liquidación....';
    const dialogRef = this.utilsService.aprobarLiquidacion(_title, _description, _waitDesciption, this.liquidacion_id, '0002');
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          this.liquidacion.getDetalleLiquidacion(this.liquidacion_id).subscribe(
            (data: any) => {
              this.dataReporte = data;
              this.listData = new MatTableDataSource(data);
              this.listData.sort = this.MatSort;
              this.listData.paginator = this.paginator;
              this.searchBan = false;
              this.update = true;
            }, ( errorServicio ) => {
              console.log(errorServicio);
            });
        }
      });
  }
 
  rechazarLiquidacion() {
    const _title: string = 'Rechazar Liquidación';
		const _description: string = '¿Estas seguro de rechazar la liquidación seleccionada?';
    const _waitDesciption: string = 'Rechazando Liquidación....';
    const dialogRef = this.utilsService.aprobarLiquidacion(_title, _description, _waitDesciption, this.liquidacion_id, '0004');
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          this.liquidacion.getDetalleLiquidacion(this.liquidacion_id).subscribe(
            (data: any) => {
              this.dataReporte = data;
              this.listData = new MatTableDataSource(data);
              this.listData.sort = this.MatSort;
              this.listData.paginator = this.paginator;
              this.searchBan = false;
              this.update = true;
            }, ( errorServicio ) => {
              console.log(errorServicio);
            });
        }
      });
  }

}
