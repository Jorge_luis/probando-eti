import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { ProcesosComponent } from '././procesos.component';

import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule,
    DateAdapter
} from '@angular/material';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CodePreviewModule } from '../../../partials/content/general/code-preview/code-preview.module';
import { PartialsModule } from '../../../partials/partials.module';
import { CoreModule } from '../../../../core/core.module';
import { MaterialPreviewModule } from '../../../partials/content/general/material-preview/material-preivew.module';
import { NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ConformidadUsuariosComponent } from './conformidad-usuarios/conformidad-usuarios.component';
import { UtilsService } from './_core/utils/utils.service';
import { DeleteEntityDialogComponent } from './_shared/delete-entity-dialog/delete-entity-dialog.component';
import { ConfirmEntityDialogComponent } from './_shared/confirm-entity-dialog/confirm-entity-dialog.component';
import { AprobacionServiciosComponent } from './aprobacion-servicios/aprobacion-servicios.component';
import { ConfirmServiceDialogComponent } from './_shared/confirm-service-dialog/confirm-service-dialog.component';
import { DeleteServiceDialogComponent } from './_shared/delete-service-dialog/delete-service-dialog.component';
import { LiquidacionEmpresaListComponent } from './liquidacion/empresa/liquidacion-empresa-list/liquidacion-empresa-list.component';
import { LiquidacionEmpresaEditComponent } from '../procesos/liquidacion/empresa/liquidacion-empresa-edit/liquidacion-empresa-edit.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ConfirmLiquidacionDialogComponent } from './_shared/confirm-liquidacion-dialog/confirm-liquidacion-dialog.component';
import { CancelLiquidacionDialogComponent } from './_shared/cancel-liquidacion-dialog/cancel-liquidacion-dialog.component';
import { UpdateTarifaDialogComponent } from './_shared/update-tarifa-dialog/update-tarifa-dialog.component';
import { LiquidacionConductorListComponent } from '../procesos/liquidacion/conductor/liquidacion-conductor-list/liquidacion-conductor-list.component';
import { LiquidacionConductorEditComponent } from './liquidacion/conductor/liquidacion-conductor-edit/liquidacion-conductor-edit.component';
import { ServiciosComponent } from './asignacion/servicios/servicios.component';
import { AsignationServiceDialogComponent } from './_shared/asignation-service-dialog/asignation-service-dialog.component';
import { AtenderObervacioneDialogComponent } from './_shared/atender-obervacione-dialog/atender-obervacione-dialog.component';



const routes: Routes = [
	{
		path: '',
		component: ProcesosComponent,
		children: [
			{
				path: 'liquidacion-empresa',
				component: LiquidacionEmpresaListComponent
            },
            {
				path: 'liquidacion-conductor',
				component: LiquidacionConductorListComponent
			},
			{
				path: 'liquidacion-empresa/add',
				component: LiquidacionEmpresaEditComponent
			},
			{
				path: 'liquidacion-empresa/view',
				component: LiquidacionEmpresaEditComponent
			},
			{
				path: 'liquidacion-empresa/view/:id',
				component: LiquidacionEmpresaEditComponent
			},
			{
				path: 'liquidacion-conductor/add',
				component: LiquidacionConductorEditComponent
			},
			{
				path: 'liquidacion-conductor/view',
				component: LiquidacionConductorEditComponent
			},
			{
				path: 'liquidacion-conductor/view/:id',
				component: LiquidacionConductorEditComponent
			},
			{
				path: 'conformidadUsuarios',
				component: ConformidadUsuariosComponent
			},
			{
				path: 'aprobacionServicios',
				component: AprobacionServiciosComponent
			},
			{
				path: 'asignacion-servicios',
				component: ServiciosComponent
            },
		]
	}
];

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};


@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		NgbModule,
		CodePreviewModule,
		CoreModule,
		MaterialPreviewModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		PerfectScrollbarModule,
		MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
		MatProgressSpinnerModule,
		MatIconModule,
		MatSelectModule,
		MatMenuModule,
		MatProgressBarModule,
		MatButtonModule,
		MatCheckboxModule,
		MatDialogModule,
		MatTabsModule,
		MatNativeDateModule,
		MatCardModule,
		MatRadioModule,
		MatDatepickerModule,
		MatAutocompleteModule,
		MatSnackBarModule,
		MatTooltipModule,
		AngularFontAwesomeModule
	],
	exports: [RouterModule],
	declarations: [
        ProcesosComponent,
		ConformidadUsuariosComponent,
		DeleteEntityDialogComponent,
		ConfirmEntityDialogComponent,
		AprobacionServiciosComponent,
		ConfirmServiceDialogComponent,
		DeleteServiceDialogComponent,
		LiquidacionEmpresaListComponent,
		LiquidacionEmpresaEditComponent,
		ConfirmLiquidacionDialogComponent,
		CancelLiquidacionDialogComponent,
		UpdateTarifaDialogComponent,
		LiquidacionConductorListComponent,
		LiquidacionConductorEditComponent,
		ServiciosComponent,
		AsignationServiceDialogComponent,
		AtenderObervacioneDialogComponent
    ],
	providers: [
		NgbAlertConfig, {
		provide: PERFECT_SCROLLBAR_CONFIG,
		useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
	  },
	  UtilsService
	],
	entryComponents: [
		DeleteEntityDialogComponent,
		ConfirmEntityDialogComponent,
		ConfirmServiceDialogComponent,
		DeleteServiceDialogComponent,
		ConfirmLiquidacionDialogComponent,
		CancelLiquidacionDialogComponent,
		UpdateTarifaDialogComponent,
		AsignationServiceDialogComponent,
		AtenderObervacioneDialogComponent
	]
})
export class ProcesosModule {
    constructor(private dateAdapter: DateAdapter<Date>) {
		dateAdapter.setLocale('en-in'); // DD/MM/YYYY
	  }
}
