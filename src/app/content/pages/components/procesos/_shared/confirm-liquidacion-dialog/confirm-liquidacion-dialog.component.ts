import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'm-confirm-liquidacion-dialog',
  templateUrl: './confirm-liquidacion-dialog.component.html',
  styleUrls: []
})
export class ConfirmLiquidacionDialogComponent implements OnInit {
	porcentaje = new FormControl('', [Validators.required, Validators.pattern(/\d+(\.\d*)?|\.\d+/)]);
  viewLoading: boolean = false;
	variable: number = 1;
  validarDelete: number;

  constructor(
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
		public dialogRef: MatDialogRef<ConfirmLiquidacionDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
		this.porcentaje.setValue('5');
  }

  onNoClick(): void {
		this.dialogRef.close();
  }

	onYesCancelClick(): void {
		this.liquidacion.registerLiquidacionConductor(this.data.entidad, this.data.viajes, this.porcentaje.value, this.data.monto, this.data.usuarioRegistro, '0003').subscribe((data: any) => {
			if (data > 0) {
				this.dialogRef.close(data);
				this.toastr.successToastr('Se realizo la liquidación correctamente.', 'Exito!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
			} else {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
			}
		}, ( errorServicio ) => {
			this.dialogRef.close(true);
			this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
				toastTimeout: 2000,
				showCloseButton: true,
				animate: 'fade',
				progressBar: true
			});
			console.log(errorServicio);
			this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
				toastTimeout: 2000,
				showCloseButton: true,
				animate: 'fade',
				progressBar: true
			});
		});
	}

  onYesClick(): void {
    if (this.data.tipo === 'E') {
      console.log('LIQUIDACION CLIENTE');
      this.liquidacion.registerLiquidacionEmpresa(this.data.entidad, this.data.viajes, this.data.monto, this.data.usuarioRegistro).subscribe((data: any) => {
				if (data > 0) {
					this.dialogRef.close(data);
					this.toastr.successToastr('Se realizo la liquidación correctamente.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
				this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
			});
    } else {
			this.liquidacion.registerLiquidacionConductor(this.data.entidad, this.data.viajes, this.porcentaje.value, this.data.monto, this.data.usuarioRegistro, '0001').subscribe((data: any) => {
				if (data > 0) {
					this.dialogRef.close(data);
					this.toastr.successToastr('Se realizo la liquidación correctamente.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
				this.toastr.errorToastr('Se produjo un error al guardar liquidación', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
			});
    }
  }
}
