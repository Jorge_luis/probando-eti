import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'm-cancel-liquidacion-dialog',
  templateUrl: './cancel-liquidacion-dialog.component.html',
  styleUrls: []
})
export class CancelLiquidacionDialogComponent implements OnInit {
  viewLoading: boolean = false;
  variable: number = 1;
  usuario: string;
  validarDelete: number;
  tipoDoc = new FormControl('', [Validators.required]);
  serie = new FormControl('', [Validators.required]);
  documento = new FormControl('', [Validators.required, Validators.pattern(/\d+(\.\d*)?|\.\d+/)]);

  arrayDoc: any[] = [
    {value: '0001', documento: 'BOLETA'},
    {value: '0002', documento: 'FACTURA'},
  ];

	constructor(
		private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
		public dialogRef: MatDialogRef<CancelLiquidacionDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
  }

  onNoClick(): void {
		this.dialogRef.close(['', true]);
	}

	onYesClick(): void {
    if (this.data.tipo === 1) {
      if (this.tipoDoc.status === 'INVALID' || this.serie.status === 'INVALID' || this.documento.status === 'INVALID') {
        this.tipoDoc.markAsTouched();
        this.serie.markAsTouched();
        this.documento.markAsTouched();
        return;
      }
    }
		/* Server loading imitation. Remove this */
		this.viewLoading = true;
    this.liquidacion.putCancelacionLiquidacion(this.data.liquidacion, this.data.tipo, this.tipoDoc.value, this.serie.value, this.documento.value, this.usuario).subscribe((data: any) => {
      console.log(data);
      if (data > 0) {
        this.dialogRef.close([data, true]);
        this.toastr.successToastr('Se cancelo la liquidación correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al cancelar la liquidación..', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      this.toastr.errorToastr('Se produjo un error al cancelar la liquidación..', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

}
