import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'm-update-tarifa-dialog',
  templateUrl: './update-tarifa-dialog.component.html',
  styleUrls: []
})
export class UpdateTarifaDialogComponent implements OnInit {
  viewLoading: boolean = false;
  usuario: string;
  monto = new FormControl('', [Validators.required, Validators.pattern(/^\d+\.\d{0,2}$/)]);


	constructor(
		private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
		public dialogRef: MatDialogRef<UpdateTarifaDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
  }

  onNoClick(): void {
		this.dialogRef.close(['', '']);
	}

	onYesClick(): void {
    if (this.monto.status === 'INVALID') {
      this.monto.markAsTouched();
      return;
    }

		/* Server loading imitation. Remove this */
		this.viewLoading = true;
    this.liquidacion.putUpdateTarifa(this.data.viaje, this.monto.value, this.usuario, this.data.codigo, this.data.tipo).subscribe((data: any) => {
      if (data[0]['cont'] > 0) {
        this.dialogRef.close([data, true]);
        this.toastr.successToastr('Se modifico correctamente la tarifa.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al modificar tarifa.', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      this.toastr.errorToastr('Se produjo un error al modificar tarifa.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  onYesConductorClick(): void {
    if (this.monto.status === 'INVALID') {
      this.monto.markAsTouched();
      return;
    }

		/* Server loading imitation. Remove this */
		this.viewLoading = true;
    this.liquidacion.putUpdateTarifa(this.data.viaje, this.monto.value, this.usuario, this.data.codigo, this.data.tipo).subscribe((data: any) => {
      if (data[0]['cont'] > 0) {
        this.dialogRef.close([data, true]);
        this.toastr.successToastr('Se modifico correctamente la tarifa.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al modificar tarifa.', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      this.toastr.errorToastr('Se produjo un error al modificar tarifa.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }


}

