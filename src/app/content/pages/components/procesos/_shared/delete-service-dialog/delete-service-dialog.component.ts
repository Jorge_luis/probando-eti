import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../../core/services/aprobacion-solicitud.service';
@Component({
  selector: 'm-delete-service-dialog',
  templateUrl: './delete-service-dialog.component.html',
  styleUrls: []
})
export class DeleteServiceDialogComponent implements OnInit {
  viewLoading: boolean = false;
	validarDelete: number;

	constructor(
		private conformidad: AprobacionSolicitudService,
    	public toastr: ToastrManager,
		public dialogRef: MatDialogRef<DeleteServiceDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	onYesClick(): void {
		this.viewLoading = true;
		this.conformidad.putRechazarServicio(this.data.Solicitud, this.data.usuarioRegistro, '0003', this.data.token, this.data.viaje).subscribe((data: any) => {
      if (data === 1) {
        this.dialogRef.close(true);
        this.toastr.successToastr('Se rechazo correctamente el servicio seleccionado.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al rechazar usuario..', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      this.toastr.errorToastr('Se produjo un error al rechazar usuario..', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
	}
}
