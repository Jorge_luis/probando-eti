import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../../core/services/aprobacion-solicitud.service';

@Component({
  selector: 'm-confirm-entity-dialog',
  templateUrl: './confirm-entity-dialog.component.html',
  styleUrls: []
})
export class ConfirmEntityDialogComponent implements OnInit {

  viewLoading: boolean = false;
	variable: number = 1;
	validarDelete: number;

	constructor(
		private conformidad: AprobacionSolicitudService,
    public toastr: ToastrManager,
		public dialogRef: MatDialogRef<ConfirmEntityDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

  ngOnInit() {
  }

  onNoClick(): void {
		this.dialogRef.close();
	}

	onYesClick(): void {
		/* Server loading imitation. Remove this */
		this.viewLoading = true;
		if (this.data.opcion === 1) {
			this.conformidad.putAceptarSolicitud(this.data.solicitud, this.data.token).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se aprobo correctamente el usuario seleccionado.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al aprobar usuario..', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al aprobar usuario..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
			});
		} else {
				this.conformidad.confirmarUsuarios(this.data.solicitud).subscribe((data: any) => {
        this.validarDelete = data.indexOf(0);
        if (this.validarDelete  === -1) {
					this.dialogRef.close(true);
          this.toastr.successToastr('Se aprobaron todos los usuarios seleccionados correctamente.', 'Exito!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (this.validarDelete  === 0) {
						this.dialogRef.close(true);
            this.toastr.warningToastr('Algunos usuarios no se rechazaron correctamente. Verificar', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
						this.dialogRef.close(true);
            this.toastr.errorToastr('Se produjo un error al rechazar usuario..', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
			});
		}
  }

}
