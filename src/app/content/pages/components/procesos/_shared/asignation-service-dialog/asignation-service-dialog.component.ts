import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'm-asignation-service-dialog',
  templateUrl: './asignation-service-dialog.component.html',
  styleUrls: []
})
export class AsignationServiceDialogComponent implements OnInit {
  viewLoading: boolean = false;
  variable: number = 1;
  usuario: string;
  validarDelete: number;
  tiempo = new FormControl('', [Validators.required, Validators.pattern(/\d+(\.\d*)?|\.\d+/)]);
  unidad = new FormControl('', [Validators.required, Validators.pattern(/\d+(\.\d*)?|\.\d+/)]);
  
  constructor(
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
		public dialogRef: MatDialogRef<AsignationServiceDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario'); 
  }

  onNoClick(): void {
		this.dialogRef.close(['', true]);
	}

	onYesClick(): void {
    if (this.data.tipo === 1) {
      if (this.unidad.status === 'INVALID' || this.tiempo.status === 'INVALID') {
        this.unidad.markAsTouched();
        this.tiempo.markAsTouched();
        return;
      }

      /* Server loading imitation. Remove this */
      this.viewLoading = true;
      this.liquidacion.putAsignacionServicio(this.data.viaje, this.unidad.value, this.data.tipo, this.tiempo.value, this.usuario, this.data.token).subscribe((data: any) => {
        if (data > 0) {
          this.dialogRef.close([data, true]);
          this.toastr.successToastr('Se asigno correctamente.', 'Exito!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (data === 0) {
            this.viewLoading = false;
            this.toastr.warningToastr('El número de placa no esta asignada.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            if (data < 0) {
              this.viewLoading = false;
              this.toastr.warningToastr('La unidad se encuentra con un servicio asignado.', 'Advertencia!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            } else {
              this.dialogRef.close(true);
              this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            }
            
          }
        }
      }, ( errorServicio ) => {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        console.log(errorServicio);
      });
    } else {
      /* Server loading imitation. Remove this */
      this.viewLoading = true;
      this.liquidacion.putAsignacionServicio(this.data.viaje, null, this.data.tipo, null, this.usuario, this.data.token).subscribe((data: any) => {
        console.log(data);
        if (data > 0) {
          this.dialogRef.close([data, true]);
          if (this.data.tipo === 2) {
            this.toastr.successToastr('Se cancelo correctamente el servicio.', 'Exito!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.toastr.successToastr('Se finalizo correctamente el servicio.', 'Exito!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        } else {
          this.dialogRef.close(true);
          if (this.data.tipo === 2) {
            this.toastr.errorToastr('Se produjo un error al cancelar el servicio.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.toastr.errorToastr('Se produjo un error al finalizar el servicio.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
      }, ( errorServicio ) => {
        this.dialogRef.close(true);
        if (this.data.tipo === 2) {
            this.toastr.errorToastr('Se produjo un error al cancelar el servicio.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.toastr.errorToastr('Se produjo un error al finalizar el servicio.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } 
        console.log(errorServicio);
      });
    }
  }
}