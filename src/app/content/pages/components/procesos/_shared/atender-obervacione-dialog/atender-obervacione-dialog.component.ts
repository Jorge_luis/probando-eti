import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'm-atender-obervacione-dialog',
  templateUrl: './atender-obervacione-dialog.component.html',
  styleUrls: ['./atender-obervacione-dialog.component.scss']
})
export class AtenderObervacioneDialogComponent implements OnInit {
  viewLoading: boolean = false;

  constructor(
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
    public dialogRef: MatDialogRef<AtenderObervacioneDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onNoClick(): void {
		this.dialogRef.close(['', true]);
  }

  onYesClick(): void {
		/* Server loading imitation. Remove this */
		this.viewLoading = true;
    this.liquidacion.putAtenderObservacionViaje(this.data.viaje).subscribe((data: any) => {
      console.log(data);
      if (data > 0) {
        this.dialogRef.close([data, true]);
        this.toastr.successToastr('Se atendio correctamente la observación.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al atender a observación.', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      this.toastr.errorToastr('Se produjo un error al atender a observación.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

}
