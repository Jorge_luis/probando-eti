import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../../core/services/aprobacion-solicitud.service';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'm-confirm-service-dialog',
  templateUrl: './confirm-service-dialog.component.html',
  styleUrls: []
})
export class ConfirmServiceDialogComponent implements OnInit {
  viewLoading: boolean = false;
	variable: number = 1;
	validarDelete: number;
  observacion = new FormControl('', []);
  filterArea = new FormControl('');

	constructor(
		private conformidad: AprobacionSolicitudService,
    public toastr: ToastrManager,
		public dialogRef: MatDialogRef<ConfirmServiceDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

  ngOnInit() {
    this.filterArea.setValue(this.data.AreaUsuario);
  }

  onNoClick(): void {
		this.dialogRef.close();
	}

	onYesClick(): void {
		/* Server loading imitation. Remove this */
		this.viewLoading = true;
		this.conformidad.putAceptarServicio(this.data.solicitud, this.data.usuarioRegistro, '0002', this.data.token, this.data.viaje, this.observacion.value, this.filterArea.value).subscribe((data: any) => {
      if (data === 1) {
        this.dialogRef.close(true);
        this.toastr.successToastr('Se aprobo correctamente el servicio seleccionado.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.dialogRef.close(true);
        this.toastr.errorToastr('Se produjo un error al aprobar servicio..', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.dialogRef.close(true);
      this.toastr.errorToastr('Se produjo un error al aprobar servicio..', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

}
