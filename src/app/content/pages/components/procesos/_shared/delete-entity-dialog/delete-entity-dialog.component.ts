import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../../core/services/aprobacion-solicitud.service';


@Component({
	selector: 'm-delete-entity-dialog',
	templateUrl: './delete-entity-dialog.component.html'
})
export class DeleteEntityDialogComponent implements OnInit {
	viewLoading: boolean = false;
	validarDelete: number;

	constructor(
		private conformidad: AprobacionSolicitudService,
    	public toastr: ToastrManager,
		public dialogRef: MatDialogRef<DeleteEntityDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }

	ngOnInit() {
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	onYesClick(): void {
		this.viewLoading = true;
		if (this.data.opcion === 1) {
			this.conformidad.putRechazoSolicitud(this.data.solicitud, this.data.token).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se rechazo correctamente el usuario.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
				  this.toastr.errorToastr('Se produjo un error al rechazar usuario..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				  });
				}
			  }, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al rechazar usuario..', 'Error!', {
				  toastTimeout: 2000,
				  showCloseButton: true,
				  animate: 'fade',
				  progressBar: true
				});
				console.log(errorServicio);
			  });
		} else {
			this.conformidad.rechazarUsuarios(this.data.solicitud).subscribe((data: any) => {
			this.validarDelete = data.indexOf(0);
			if (this.validarDelete  === -1) {
				this.dialogRef.close(true);
			  	this.toastr.successToastr('Se rechazó correctamente todos los usuarios seleccionados.', 'Exito!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
			  	});
			} else {
			  if (this.validarDelete  === 0) {
				this.toastr.warningToastr('Algunos usuarios no se rechazaron correctamente. Verificar', 'Advertencia!', {
				  toastTimeout: 2000,
				  showCloseButton: true,
				  animate: 'fade',
				  progressBar: true
				});
			  } else {
				this.toastr.errorToastr('Se produjo un error al rechazar usuario..', 'Error!', {
				  toastTimeout: 2000,
				  showCloseButton: true,
				  animate: 'fade',
				  progressBar: true
				});
			  }
			}
				});
		}
	}
}
