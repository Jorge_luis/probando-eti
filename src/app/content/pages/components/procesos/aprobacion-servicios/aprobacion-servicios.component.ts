import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BehaviorSubject, Observable, interval } from 'rxjs';
import { DatePipe } from '@angular/common';
import { UtilsService } from '../_core/utils/utils.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../core/services/aprobacion-solicitud.service';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'm-aprobacion-servicios',
  templateUrl: './aprobacion-servicios.component.html',
  styleUrls: ['./aprobacion-servicios.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class AprobacionServiciosComponent implements OnInit {

  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['dni', 'solicitante', 'fecha', 'hora', 'origen', 'destino', 'tarifa', 'estado', 'actions'];
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  searchBan: boolean = false;
  array: any[] = [];
  areaCodigo: string;
  aprobador: string;
  searchKey: string;
  arrayAreas: any[] = [];
  selection = new SelectionModel(true, []);
  filtroEstado: string = '';
  array2: any[] = [];
  totalInicial: number;
  totalActual: number;

  constructor(
    private subheaderService: SubheaderService,
    private datePipe: DatePipe,
    private utilsService: UtilsService,
    private conformidad: AprobacionSolicitudService,
    public toastr: ToastrManager,
  ) { }

  ngOnInit() {
    this.subheaderService.setTitle('Aprobación de Usuarios');
    console.log(localStorage);
    this.aprobador = localStorage.getItem('aprobador');
    this.areaCodigo = localStorage.getItem('cliente');
    this.getAreas(this.areaCodigo);
    this.searchSolicitudServiciosCorporativos();
  }

  getAreas(Area: string) {
    this.conformidad.getServicioAreas(Area).subscribe(
      (data: any) => {
          this.arrayAreas = data;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.searchBan = false;
          this.toastr.errorToastr('Ocurrio un error al cargar serivicios', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
        });
  }

  searchSolicitudServiciosCorporativos() {
    this.searchBan = true;
    this.conformidad.getServiciosCorporativos(this.areaCodigo, '0001').subscribe(
      (data: any) => {
        this.array = data;
        this.totalInicial = this.array.length;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        const secondsCounter = interval(8000);
// Subscribe to begin publishing values
        secondsCounter.subscribe(n =>
          this.actualizacionBusqueda());
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.searchBan = false;
          this.toastr.errorToastr('Ocurrio un error al cargar serivicios', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
        });
  }

  actualizacionBusqueda() {
    this.searchBan = true;
    this.conformidad.getServiciosCorporativos(this.areaCodigo, '0001').subscribe(
      (data: any) => {
        this.array2 = data;
        this.totalActual = this.array2.length;
        this.listData = new MatTableDataSource(this.array2);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        if (this.totalInicial !== this.totalActual) {
          const audio = new Audio('../../../assets/app/media/img/alerts/ALERTA_SERVICIO.mp3');
          audio.play();
          this.totalInicial = this.array2.length;
      }
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Ocurrio un error al cargar serivicios', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
        });
  }

  getSolicitudCssClassByEstado(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'warning';
		}
		return '';
  }

  getSolicitudEstadoString(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'Pendiente';
		}
		return '';
  }

  filterServicios() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  aceptarServicio(Solicitud: number, Token: string, Viaje: number, Area: number) {
		const _title: string = 'Aprobación de Servicio';
		const _description: string = '¿Estás seguro de aprobar el servicio seleccionado?. Verifique el centro de costos y si tiene alguna observación por favor ingresarlo, caso contrario dejar vacio el campo observación.';
		const _waitDesciption: string = 'Servicio en aprobación...';
		const _deleteMessage = `Servicio aprobado correctamente.`;

		const dialogRef = this.utilsService.confirmServices(_title, _description, _waitDesciption, Solicitud, this.aprobador, Token, Viaje, this.arrayAreas, Area);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.actualizacionBusqueda();
      }
		});
  }

  rechazarServicio(Solicitud: number, Token: string, Viaje: number) {
		const _title: string = 'Rechazar Servicio';
		const _description: string = '¿Estás seguro de rechazar al servicio seleccionado?';
		const _waitDesciption: string = 'Rechazando servicio..';

		const dialogRef = this.utilsService.deleteServices(_title, _description, _waitDesciption, Solicitud, this.aprobador, Token, Viaje);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			} else {
        this.actualizacionBusqueda();
      }
		});
  }

}
