import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BehaviorSubject, Observable, interval } from 'rxjs';
import { DatePipe } from '@angular/common';
import { UtilsService } from '../../_core/utils/utils.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../../core/services/aprobacion-solicitud.service';

@Component({
  selector: 'm-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class ServiciosComponent implements OnInit {
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['empresa', 'solicitante', 'telefono', 'fechasolicitud', 'horasolicitud', 'unidad', 'origen', 'destino', 'estado', 'tarifa', 'actions'];
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  searchBan: boolean = false;
  array: any[] = [];
  array2: any[] = [];
  searchKey: string;
  filtroEstado: string = '';
  totalInicial: number;
  totalActual: number;

  constructor(
    private subheaderService: SubheaderService,
    private datePipe: DatePipe,
    private utilsService: UtilsService,
    private conformidad: AprobacionSolicitudService,
    public toastr: ToastrManager,
  ) { }

  ngOnInit() {
    this.subheaderService.setTitle('Asignación de Servicios');
    this.searchSolicitudServiciosCorporativos();
  }

  searchSolicitudServiciosCorporativos() {
    this.searchBan = true;
    this.conformidad.getViajesSolicitados().subscribe(
      (data: any) => {
        this.array = data;
        this.totalInicial = this.array.length;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        const secondsCounter = interval(7000);
        secondsCounter.subscribe(n =>
          this.actualizacionBusqueda());
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.searchBan = false;
          this.toastr.errorToastr('Ocurrio un error al cargar serivicios', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
        });
  }

  actualizacionBusqueda() {
    this.searchBan = true;
    this.conformidad.getViajesSolicitados().subscribe(
      (data: any) => {
        this.array2 = data;
        this.totalActual = this.array2.length;
        this.listData = new MatTableDataSource(this.array2);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        if (this.totalInicial !== this.totalActual) {
            const audio = new Audio('../../../assets/app/media/img/alerts/ALERTA_SERVICIO.mp3');
            audio.play();
            this.totalInicial = this.array2.length;
        }
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Ocurrio un error al cargar servicios', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
        });
  }

  getSolicitudCssClassByEstado(status: string = ''): string {
		switch (status) {
			case '0002':
        return 'warning';
      case '0003':
        return 'default';
      case '0004':
        return 'primary';
      case '0005':
        return 'primary';
      case '0006':
        return 'success';
      case '0007':
        return 'danger';
		}
		return '';
  }

  getSolicitudEstadoString(status: string = ''): string {
		switch (status) {
			case '0002':
        return 'Solicitado';
      case '0003':
        return 'En camino';
      case '0004':
        return 'En el punto';
      case '0005':
        return 'En viaje';
      case '0006':
        return 'Finalizado';
      case '0007':
        return 'Cancelado';
		}
		return '';
  }

  filterServicios() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  aceptarAsignacion(Viaje: number, Token: string) {
		const _title: string = 'Asignación  de Servicio';
		const _description: string = 'Seleccione un conductor y el tiempo de demora.';
		const _waitDesciption: string = 'Asignando servicio...';

		const dialogRef = this.utilsService.asignacionServicio(_title, _description, _waitDesciption, Viaje, Token, 1);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.actualizacionBusqueda();
      }
		});
  }

  rechazarAsignacion(Viaje: number, Token: string) {
		const _title: string = 'Cancelar el servicio solicitado';
		const _description: string = '¿Estás seguro de cancelar al servicio seleccionado?';
		const _waitDesciption: string = 'Cancelando Servicio....';

		const dialogRef = this.utilsService.asignacionServicio(_title, _description, _waitDesciption, Viaje, Token, 2);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			} else {
        this.actualizacionBusqueda();
      }
		});
  }

  terminarViaje(Viaje: number, Token: string) {
		const _title: string = 'Finalizar el servicio solicitado';
		const _description: string = '¿Estás seguro de finalizar al servicio seleccionado?';
		const _waitDesciption: string = 'Finalizando Servicio....';

		const dialogRef = this.utilsService.asignacionServicio(_title, _description, _waitDesciption, Viaje, Token, 3);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			} else {
        this.actualizacionBusqueda();
      }
		});
  }

}
