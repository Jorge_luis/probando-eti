import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';


import { DeleteEntityDialogComponent } from '../../_shared/delete-entity-dialog/delete-entity-dialog.component';
import { ConfirmEntityDialogComponent } from '../../_shared/confirm-entity-dialog/confirm-entity-dialog.component';
import { ConfirmServiceDialogComponent } from '../../_shared/confirm-service-dialog/confirm-service-dialog.component';
import { DeleteServiceDialogComponent } from '../../_shared/delete-service-dialog/delete-service-dialog.component';
import { ConfirmLiquidacionDialogComponent } from '../../_shared/confirm-liquidacion-dialog/confirm-liquidacion-dialog.component';
import { CancelLiquidacionDialogComponent } from '../../_shared/cancel-liquidacion-dialog/cancel-liquidacion-dialog.component';
import { UpdateTarifaDialogComponent } from '../../_shared/update-tarifa-dialog/update-tarifa-dialog.component';
import { AsignationServiceDialogComponent } from '../../_shared/asignation-service-dialog/asignation-service-dialog.component';
import { AtenderObervacioneDialogComponent } from '../../_shared/atender-obervacione-dialog/atender-obervacione-dialog.component';


export enum MessageType {
	Create,
	Read,
	Update,
	Delete
}

@Injectable()
export class UtilsService {
  constructor(
    private dialog: MatDialog
  ) {}

  	/* METODOS DE APROBACION DE USUARIOS */
	deleteElement(title: string = '', description: string = '', waitDesciption: string = '', solicitud: number, opcion: number, token: string) {
		return this.dialog.open(DeleteEntityDialogComponent, {
			data: { title, description, waitDesciption,  solicitud, opcion, token},
			width: '440px'
		});
	}

	deleteElements(title: string = '', description: string = '', waitDesciption: string = '', solicitud: number[], opcion: number) {
		return this.dialog.open(DeleteEntityDialogComponent, {
			data: { title, description, waitDesciption,  solicitud, opcion},
			width: '440px'
		});
	}

	// Method returns instance of MatDialog - Confirmar
	confirmElement(title: string = '', description: string = '', waitDesciption: string = '', solicitud: number, opcion: number, token: string) {
		return this.dialog.open(ConfirmEntityDialogComponent, {
			data: { title, description, waitDesciption, solicitud, opcion, token},
			width: '440px'
		});
	}

	confirmElements(title: string = '', description: string = '', waitDesciption: string = '', solicitud: number[], opcion: number) {
		return this.dialog.open(ConfirmEntityDialogComponent, {
			data: { title, description, waitDesciption, solicitud, opcion},
			width: '440px'
		});
	}

	/* METODOS DE APROBACION DE SERVICIOS CORPORATIVOS */
	confirmServices(title: string = '', description: string = '', waitDesciption: string = '', solicitud: number, usuarioRegistro: string, token: string, viaje: number, Areas: any, AreaUsuario: number) {
		return this.dialog.open(ConfirmServiceDialogComponent, {
			data: { title, description, waitDesciption, solicitud, usuarioRegistro, token, viaje, Areas, AreaUsuario},
			width: '440px'
		});
	}

	deleteServices(title: string = '', description: string = '', waitDesciption: string = '', Solicitud: number, usuarioRegistro: string, token: string, viaje: number) {
		return this.dialog.open(DeleteServiceDialogComponent, {
			data: { title, description, waitDesciption,  Solicitud, usuarioRegistro, token, viaje},
			width: '440px'
		});
	}

	/* METODOS DE REGISTRO DE LIQUIDACION */
	confirmLiquidacion(title: string = '', description: string = '', waitDesciption: string = '', entidad: string, viajes: string, monto: number, usuarioRegistro: string, tipo: string) {
		return this.dialog.open(ConfirmLiquidacionDialogComponent, {
			data: { title, description, waitDesciption, entidad, viajes, monto, usuarioRegistro, tipo},
			width: '440px'
		});
	}

	cancelLiquidacion(title: string = '', description: string = '', waitDesciption: string = '', liquidacion: number, tipo: string) {
		return this.dialog.open(CancelLiquidacionDialogComponent, {
			data: { title, description, waitDesciption, liquidacion, tipo},
			width: '500px'
		});
	}

	updateTarifa(title: string = '', description: string = '', waitDesciption: string = '', viaje: number, codigo: number, tipo: number) {
		return this.dialog.open(UpdateTarifaDialogComponent, {
			data: { title, description, waitDesciption, viaje, codigo, tipo},
			width: '500px'
		});
	}

	/*	ASIGNACION DE SERVICIO A CONDUCTOR	*/
	asignacionServicio(title: string = '', description: string = '', waitDesciption: string = '', viaje: number, token: string, tipo: number) {
		return this.dialog.open(AsignationServiceDialogComponent, {
			data: { title, description, waitDesciption, viaje, token, tipo},
			width: '400px',
		});
	}

	atenderObservacion(title: string = '', description: string = '', waitDesciption: string = '', viaje: number) {
		return this.dialog.open(AtenderObervacioneDialogComponent, {
			data: { title, description, waitDesciption, viaje},
			width: '400px',
		});
	}
}
