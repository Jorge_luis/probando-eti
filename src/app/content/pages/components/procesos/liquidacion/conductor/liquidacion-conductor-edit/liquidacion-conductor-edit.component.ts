import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy, Injectable } from '@angular/core';
import { SubheaderService } from '../../../../../../../core/services/layout/subheader.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../../core/services/liquidacion.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { UtilsService } from '../../../_core/utils/utils.service';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import * as ExcelProper from 'ExcelJS';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'm-liquidacion-conductor-edit',
  templateUrl: './liquidacion-conductor-edit.component.html',
  styleUrls: ['./liquidacion-conductor-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class LiquidacionConductorEditComponent implements OnInit {
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  searchBan: boolean = false;
  banNuevo: boolean = true;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  conductor = new FormControl('', [Validators.required]);
  arrayConductores: any[] = [];
  filteredOptions: Observable<string[]>;
  filtroConductor: string;
  selection = new SelectionModel(true, []);
  viajesResult: any[] = [];
  usuario: string;
  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['select', 'atencionobservacion', 'fechasolicitud', 'fechainicioviaje', 'fechafinviaje', 'solicitante', 'origen', 'destino',  'unidad',  'placa', 'cliente', 'monto', 'montoliquidado', 'actions'];
  isValid: boolean = false;
  isReset: boolean = true;
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  nroLiquidacion: string;
  total: number = 0;
  nroLiquidacion_view: string;
  fecha_view: string;
  conductor_view: string;
  pagar_view: string;
  total_view: string;
  liquidacion_id: number;
  dataReporte: any[] = [];
  isDetails: boolean = false;
  sName: string;
  excelFileName: string;
  codConductor: number;
  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  cols = ['Fecha', 'H. Inicio Servicio', 'H. Fin Servicio', 'Empresa', 'Solicitante', 'Area', 'Ce.Co', 'Origen', 'Destino', 'Móvil', 'Placa', 'Tarifa (S/)'];

  constructor(
    private router: Router,
    private subheaderService: SubheaderService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private liquidacion: LiquidacionService,
    private utilsService: UtilsService,
    private activatedRoute: ActivatedRoute,
  ) {  }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
    this.activatedRoute.queryParams.subscribe(params => {
      const id = +params.id;
      this.liquidacion_id = id;
			if (id && id > 0) {
        this.subheaderService.setTitle('Detalles de Liquidación');
        this.banNuevo = false;
				this.initLiquidacion(id);
			} else {
        this.liquidacion.getCodigoLiquidacion('0002').subscribe(
          (data: any) => {
              this.nroLiquidacion = data[0]['codigo'];
            }, ( errorServicio ) => {
              console.log(errorServicio);
            });

        this.isDetails = true;
        this.initLiquidacion(id);
        this.subheaderService.setTitle('Nueva Liquidación');
        this.liquidacion.getConductores('').subscribe(
          (data: any) => {
              this.arrayConductores = data;
              this.filteredOptions = this.conductor.valueChanges
                .pipe(
                  startWith(''),
                  map(value => this._filter(value))
                );
                console.log(this.filteredOptions);
            }, ( errorServicio ) => {
              console.log(errorServicio);
            });
        this.array = [];
        this.listData = new MatTableDataSource(this.array);
			}
    });
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.arrayConductores.filter(arrayConductor => (arrayConductor.nombres).toLowerCase().includes(filterValue));
  }

  goBack(id = 0) {
		let _backUrl = 'procesos/liquidacion-conductor';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
  }

  loadLiquidacionFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadLiquidacionFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }

  searchViajesConductor() {
    if (this.conductor.status === 'INVALID') {
      this.conductor.markAsTouched();
      return;
    } else {
      const conductorId = this.arrayConductores.find(conduc => conduc.nombres === this.conductor.value);
      if (conductorId !== undefined) {
        this.filtroConductor = conductorId['conductor'];
      } else {
        this.array = [];
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.toastr.warningToastr('El conductor ingresado no es el correcto.', 'Alerta!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        return;
      }
    }

    this.searchBan = true;
    this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
    this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));

    this.liquidacion.getServiciosConductor(this.filtroConductor, this.filtroFechaInicio, this.filtroFechaFin).subscribe(
      (data: any) => {
        this.array = data;
        this.viajesResult = data;
        if (this.array.length > 0) {
          this.isValid = true;
          this.isReset = false;
        }
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  initLiquidacion(id: number) {
    if (id > 0) {
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Procesos', page: '/procesos/liquidacion-conductor' },
        { title: 'Liqu. Conductor',  page: '/procesos/liquidacion-conductor' },
        { title: 'Detalle Liquidación', page: '/procesos/liquidacion-conductor/view', queryParams: { id: id } }
      ]);

      this.liquidacion.getDatosLiquidacion(id).subscribe(
        (data: any) => {
          this.nroLiquidacion_view = data[0]['codigoLiquidacion'];
          this.conductor_view = data[0]['nombres'];
          this.pagar_view = data[0]['montopagar'];
          this.total_view = data[0]['total'];
          this.fecha_view = data[0]['fechaLiquidacion'];
          this.codConductor = data[0]['conductor'];
          if (data[0]['estado'] === '0002' || data[0]['estado'] === '0003' || data[0]['estado'] === '0004') {
            this.isDetails = false;
          } else {
            this.isDetails = true;
          }
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
        this.searchBan = true;
        this.liquidacion.getDetalleLiquidacion(id).subscribe(
          (data: any) => {
            this.dataReporte = data;
            this.listData = new MatTableDataSource(data);
            this.listData.sort = this.MatSort;
            this.listData.paginator = this.paginator;
            this.searchBan = false;
          }, ( errorServicio ) => {
            console.log(errorServicio);
          });


      return;
    } else {
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Procesos', page: '/procesos/liquidacion-conductor' },
        { title: 'Liqu. Conductor',  page: '/procesos/liquidacion-conductor' },
        { title: 'Nueva Liquidación', page: '/procesos/liquidacion-conductor/add' }
      ]);
      return;
    }
  }

  getDetalleCssClassByEstado(status: boolean): string {
    switch (status) {
      case false:
        return 'danger';
    }
    return '';
  }

  getDetalleEstadoString(status: boolean): string {
    switch (status) {
      case false:
        return 'Observado';
    }
    return '';
  }

  getComponentTitle() {
		let result = 'Nueva Liquidación';
		if (!this.liquidacion_id) {
			return result;
		}

		result = `Detalle de Liquidación`;
		return result;
  }

  reset() {
    this.isReset = true;
    this.isValid = false;
    this.total = 0;
    this.array = [];
    this.listData = new MatTableDataSource(this.array);
    this.conductor.reset('');
    this.filterFechaIni = null;
    this.filterFechaFin = null;
    this.viajesResult = [];
    this.selection.clear();
  }

	isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.viajesResult.length;
    this.total = 0;
    this.total = this.selection.selected.map(t => t.montoliquidado).reduce((acc, value) => acc + value, 0);
    return numSelected === numRows;
  }

  masterToggle() {
		if (this.isAllSelected()) {
      this.selection.clear();
      this.total = 0;
		} else {
      this.viajesResult.forEach(lesson => this.selection.select(lesson));
      this.total = this.viajesResult.map(t => t.montoliquidado).reduce((acc, value) => acc + value, 0);
		}
  }

  saveLiquidacion() {
    // tslint:disable-next-line:quotemark
    let xmlViajes: string = "<?xml version='1.0' encoding='ISO-8859-1'?><ROOT>";
    for (let i = 0; i < this.selection.selected.length; i++) {
      // tslint:disable-next-line:quotemark
      xmlViajes += "<Viajes ";
      // tslint:disable-next-line:quotemark
      xmlViajes += "ViajeId='" + this.selection.selected[i].viaje + "' />";
    }
    // tslint:disable-next-line:quotemark
    xmlViajes += "</ROOT>";
    const _title: string = 'Registro de Liquidación';
		const _description: string = '¿Estas seguro de realizar la liquidación?';
		const _waitDesciption: string = 'Registrando Liquidación';

		const dialogRef = this.utilsService.confirmLiquidacion(_title, _description, _waitDesciption, this.filtroConductor, xmlViajes, this.total, this.usuario, 'C');
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        if (res > 0) {
          const _refreshUrl = 'procesos/liquidacion-conductor/view?id=' + res;
		      this.router.navigateByUrl(_refreshUrl);
        }
      }
    });
  }

  downloadEXCEL() {
    console.log(this.dataReporte);
    this.sName = `${ this.nroLiquidacion_view }`;
    this.excelFileName = `Liquidacion-${ this.nroLiquidacion_view }.xlsx`;
    const workbook = new Excel.Workbook();
    workbook.creator = 'Web';
    workbook.lastModifiedBy = 'Web';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.addWorksheet(this.sName, { views: [{ state: 'frozen', ySplit: 0, xSplit: 20, activeCell: 'A1', showGridLines: true }] });
    const sheet = workbook.getWorksheet(1);
    sheet.addRow([`N° lIQUIDACION:`, `${ this.nroLiquidacion_view }`, '', `Conductor:`, `${ this.conductor_view }`]);
    sheet.addRow([`Fecha Liquidación:`, `${ this.datePipe.transform(this.fecha_view, 'dd-MM-yyyy') }`, '', `Monto a Pagar:`, `${ this.pagar_view }`, '', `Monto Total:`, `${ this.total_view }`]);
    sheet.getColumn(4).width = 30;
    sheet.getRow(4).values = this.cols;

    sheet.columns = [
      { key: 'fecha', width: 12 },
      { key: 'horainicio', width: 12 },
      { key: 'horafin', width: 12 },
      { key: 'cliente', width: 30 },
      { key: 'solicitante', width: 30 },
      { key: 'area', width: 28 },
      { key: 'centrocostos', width: 20 },
      { key: 'origen', width: 30 },
      { key: 'destino', width: 30 },
      { key: 'unidad' },
      { key: 'placa' },
      { key: 'montoliquidado' }
    ];

    ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4', 'J4', 'K4', 'L4', 'M4'].map(key => {
      sheet.getCell(key).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'EDFC00' }
      };
      });

    sheet.addRows(this.dataReporte);
    workbook.xlsx.writeBuffer().then(dataReporte => {
      const blob = new Blob([dataReporte], { type: this.blobType });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.href = url;
      a.download = this.excelFileName;
      a.click();
    });
  }

  updateTarifa(Viaje: number) {
    console.log(Viaje);
    const _title: string = 'Modificar Tarifa';
		const _description: string = '¿Estas seguro de realizar de modificar la tarifa inicial?';
    const _waitDesciption: string = 'Modificando tarifa....';
    if (this.liquidacion_id > 0) {
      const dialogRef = this.utilsService.updateTarifa(_title, _description, _waitDesciption, Viaje, this.liquidacion_id, 3);
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          this.total_view = res[0][0]['total'];
          this.pagar_view = res[0][0]['totalpagar'];
          this.liquidacion.getDetalleLiquidacion(this.liquidacion_id).subscribe(
            (data: any) => {
              this.dataReporte = data;
              this.listData = new MatTableDataSource(data);
              this.listData.sort = this.MatSort;
              this.listData.paginator = this.paginator;
              this.searchBan = false;
            }, ( errorServicio ) => {
              console.log(errorServicio);
              this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            });
        }
      });
    } else {
      const dialogRef = this.utilsService.updateTarifa(_title, _description, _waitDesciption, Viaje, Viaje, 4);
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          this.searchViajesConductor();
          this.selection.clear();
          this.total = 0;
        }
      });
    }
    return;
  }

}
