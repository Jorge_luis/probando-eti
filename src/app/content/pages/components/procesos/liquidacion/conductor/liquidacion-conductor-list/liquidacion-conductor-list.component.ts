import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, Pipe, PipeTransform} from '@angular/core';
import { EmpresasService } from '../../../../../../../core/services/empresas.service';
import { LiquidacionService } from '../../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { SubheaderService } from '../../../../../../../core/services/layout/subheader.service';
import { UtilsService } from '../../../_core/utils/utils.service';

import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'm-liquidacion-conductor-list',
  templateUrl: './liquidacion-conductor-list.component.html',
  styleUrls: ['./liquidacion-conductor-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class LiquidacionConductorListComponent implements OnInit {
  conductor = new FormControl();
  options: any[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  searchBan: boolean = false;
  searchKey: string;
  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['fechaliquidacion', 'codigoliquidacion', 'dni', 'cliente', 'montopago', 'total', 'estado', 'actions'];
  arrayConductores: any[] = [];
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  filtroConductor: number;



  constructor(
    private empresa: EmpresasService,
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private subheaderService: SubheaderService,
    private utilsService: UtilsService,
  ) { }

  ngOnInit() {
    this.subheaderService.setTitle('Liquidación de Conductores');
    this.liquidacion.getConductores('').subscribe(
      (data: any) => {
          this.arrayConductores = data;
          this.filteredOptions = this.conductor.valueChanges
            .pipe(
              startWith(''),
              map(value => this._filter(value))
            );

            console.log(this.filteredOptions);
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });

    this.listData = new MatTableDataSource(this.array);
    this.listData.sort = this.MatSort;
    this.listData.paginator = this.paginator;
    this.searchLiquidacion();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.arrayConductores.filter(arrayConductor => (arrayConductor.nombres).toLowerCase().includes(filterValue));
  }

  loadLiquidacionFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadLiquidacionFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }


  searchLiquidacion() {
    if (this.conductor.value === null || this.conductor.value === '') {
      this.filtroConductor = null;
    } else {
      const conductorId = this.arrayConductores.find(conduc => conduc.nombres === this.conductor.value);
      if (conductorId !== undefined) {
        this.filtroConductor = conductorId['conductor'];
      } else {
        this.array = [];
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.toastr.warningToastr('El conductor ingresado no es el correcto.', 'Alerta!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        return;
      }
    }

    this.searchBan = true;
    this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
    this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));
    this.liquidacion.getLiquidacion('0002', this.filtroConductor, this.filtroFechaInicio, this.filtroFechaFin).subscribe(
      (data: any) => {
        this.array = data;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Ocurrio un problema al listar liquidaciones, por favor intente nuevamente.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });

  }

  getEmpresaCssClassByEstado(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'warning';
      case '0002':
        return 'primary';
      case '0003':
				return 'success';
			case '0004':
				return 'danger';
		}
		return '';
  }

  getEmpresaEstadoString(status: string = ''): string {
		switch (status) {
			case '0001':
        return 'Pendiente';
      case '0002':
        return 'Aprobado';
      case '0003':
				return 'Pagado';
			case '0004':
				return 'Rechazado';
		}
		return '';
  }

  filterLiquidaciones() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  cancelarLiquidacion(Liquidacion: number) {
		const _title: string = 'Realizar Cancelación de Liquidación';
		const _description: string = '¿Esta seguro de cancelar la liquidación?';
		const _waitDesciption: string = 'Registrando pago de liquidación....';

		const dialogRef = this.utilsService.cancelLiquidacion(_title, _description, _waitDesciption, Liquidacion, '2');
		dialogRef.afterClosed().subscribe(res => {
      console.log(res);
			if (!res[1]) {
				return;
			} else {
        console.log(res);
        this.searchLiquidacion();
      }
		});
  }

}

