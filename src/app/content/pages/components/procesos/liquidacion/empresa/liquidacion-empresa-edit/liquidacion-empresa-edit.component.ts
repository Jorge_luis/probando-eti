import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy, Injectable } from '@angular/core';
import { SubheaderService } from '../../../../../../../core/services/layout/subheader.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { EmpresasService } from '../../../../../../../core/services/empresas.service';
import { startWith, map } from 'rxjs/operators';
import { ToastrManager } from 'ng6-toastr-notifications';
import { LiquidacionService } from '../../../../../../../core/services/liquidacion.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { UtilsService } from '../../../_core/utils/utils.service';
import { BrowserModule } from '@angular/platform-browser';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import * as ExcelProper from 'ExcelJS';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'm-liquidacion-empresa-edit',
  templateUrl: './liquidacion-empresa-edit.component.html',
  styleUrls: ['./liquidacion-empresa-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class LiquidacionEmpresaEditComponent implements OnInit {

  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  searchBan: boolean = false;
  banNuevo: boolean = true;
  filterCliente: string;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  productForm: FormGroup;
  filterEmpresa = new FormControl('', [Validators.required]);
  arrayEmpresas: any[] = [];
  selectedEmpresa: number = 1;
  filteredManufactures: Observable<string[]>;
  array: any[] = [];
  clienteId: string;
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['select', 'atencionobservacion', 'fechasolicitud', 'fechainicioviaje', 'solicitante', 'origen', 'destino',  'unidad',  'placa', 'conductor', 'observacion', 'monto', 'montoliquidado', 'actions'];
  filtroEmpresa: number;
  selection = new SelectionModel(true, []);
  serviciosResult: any[] = [];
  serviciosCont: any[] = [];
  nroLiquidacion: string;
  isValid: boolean = false;
  isReset: boolean = true;
  total: number = 0;
  nroLiquidacion_view: string;
  fecha_view: string;
  empresa_view: string;
  total_view: string;
  liquidacion_id: number;
  dataReporte: any[] = [];
  isDetails: boolean = false;
  name: string;
  sName: string;
  excelFileName: string;
  codEmpresa: number;
  usuario: string;

  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  cols = ['Fecha', 'H. Inicio Servicio', 'H. Fin Servicio', 'Solicitante', 'Area', 'Ce.Co', 'Origen', 'Destino', 'Móvil', 'Placa', 'Conductor', 'Tarifa (S/)'];

  constructor(
    private empresa: EmpresasService,
    private router: Router,
    private subheaderService: SubheaderService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private liquidacion: LiquidacionService,
    private utilsService: UtilsService,
    private activatedRoute: ActivatedRoute
  ) {  }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
    this.activatedRoute.queryParams.subscribe(params => {
      const id = +params.id;
      this.liquidacion_id = id;
			if (id && id > 0) {
        this.subheaderService.setTitle('Detalles de Liquidación');
        this.banNuevo = false;
				this.initLiquidacion(id);
			} else {
        this.liquidacion.getCodigoLiquidacion('0001').subscribe(
          (data: any) => {
              this.nroLiquidacion = data[0]['codigo'];
            }, ( errorServicio ) => {
              console.log(errorServicio);
            });

        this.isDetails = true;
        this.initLiquidacion(id);
        this.subheaderService.setTitle('Nueva Liquidación');
        this.array = [];
        this.listData = new MatTableDataSource(this.array);
			}
    });
  }

  initLiquidacion(id: number) {
    if (id > 0) {
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Procesos', page: '/procesos/liquidacion-empresa' },
        { title: 'Liqu. Empresas',  page: '/procesos/liquidacion-empresa' },
        { title: 'Detalle Liquidación', page: '/procesos/liquidacion-empresa/view', queryParams: { id: id } }
      ]);

      this.liquidacion.getDatosLiquidacion(id).subscribe(
        (data: any) => {
          this.nroLiquidacion_view = data[0]['codigoLiquidacion'];
          this.empresa_view = data[0]['razonSocial'];
          this.total_view = data[0]['total'];
          this.fecha_view = data[0]['fechaLiquidacion'];
          this.codEmpresa = data[0]['empresa'];
          if (data[0]['estado'] === '0002' || data[0]['estado'] === '0003' || data[0]['estado'] === '0004') {
            this.isDetails = false;
          } else {
            this.isDetails = true;
          }
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
        this.searchBan = true;
        this.liquidacion.getDetalleLiquidacion(id).subscribe(
          (data: any) => {
            this.dataReporte = data;
            this.listData = new MatTableDataSource(data);
            this.listData.sort = this.MatSort;
            this.listData.paginator = this.paginator;
            this.searchBan = false;
          }, ( errorServicio ) => {
            console.log(errorServicio);
          });


      return;
    } else {
      this.createForm();
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Procesos', page: '/procesos/liquidacion-empresa' },
        { title: 'Liqu. Empresas',  page: '/procesos/liquidacion-empresa' },
        { title: 'Nueva Liquidación', page: '/procesos/liquidacion-empresa/add' }
      ]);
      return;
    }
  }

  getDetalleCssClassByEstado(status: boolean): string {
    if (this.liquidacion_id > 0) {
      switch (status) {
        case true:
          return 'primary';
        case false:
          return 'warning';
      }
      return 'default';
    } else {
      switch (status) {
        case false:
          return 'danger';
      }
      return 'default';
    }
  }

  getDetalleEstadoString(status: boolean): string {
    if (this.liquidacion_id > 0) {
      switch (status) {
        case true:
          return 'Atendido';
        case false:
          return 'Pendiente';
      }
      return 'Sin Obs.';
    } else {
      switch (status) {
        case true:
          return 'Sin Obs.';
        case false:
          return 'Observado';
      }
      return 'Sin Obs.';
    }
  }

  createForm() {
    this.empresa.getEmpresas(1).subscribe(
      (data: any) => {
          this.arrayEmpresas = data;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Se produjo un error al cargar empresa, por favor refresque la pantalla.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  onChangeEmpresa(cliente: string) {
    this.searchViajesCorporativos(cliente);
    this.clienteId = cliente;
  }

  searchViajesCorporativos(cliente: string) {
      this.searchBan = true;
      this.liquidacion.getServiciosCorporativos(cliente).subscribe(
        (data: any) => {
          this.array = data;
          this.serviciosResult = data;
          if (this.array.length > 0) {
            this.isValid = true;
            this.isReset = false;
          }
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
          this.searchBan = false;
          }, ( errorServicio ) => {
            console.log(errorServicio);
            this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          });
  }

  getComponentTitle() {
		let result = 'Nueva Liquidación';
		if (!this.liquidacion_id) {
			return result;
		}

		result = `Detalle de Liquidación`;
		return result;
  }

  goBack(id = 0) {
		let _backUrl = 'procesos/liquidacion-empresa';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
  }

	isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.serviciosResult.length;
    this.total = 0;
    this.total = this.selection.selected.map(t => t.montoliquidado).reduce((acc, value) => acc + value, 0);
    return numSelected === numRows;
  }

  masterToggle() {
		if (this.isAllSelected()) {
      this.selection.clear();
      this.total = 0;
		} else {
      this.serviciosResult.forEach(lesson => this.selection.select(lesson));
      this.total = this.serviciosResult.map(t => t.montoliquidado).reduce((acc, value) => acc + value, 0);
		}
  }

  reset() {
    this.isReset = true;
    this.isValid = false;
    this.total = 0;
    this.array = [];
    this.listData = new MatTableDataSource(this.array);
    this.filterCliente = '';
    this.serviciosResult = [];
    this.selection.clear();
  }

  atenderObservacion(Viaje: number) {
    const _title: string = 'Atender Observación';
		const _description: string = '¿Estas seguro de dar por atendida la observación?';
    const _waitDesciption: string = 'Atendiendo observación....';
    const dialogRef = this.utilsService.atenderObservacion(_title, _description, _waitDesciption, Viaje);
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          console.log(this.clienteId);
          if (this.liquidacion_id > 0) {
            this.searchBan = true;
            this.liquidacion.getDetalleLiquidacion(this.liquidacion_id).subscribe(
              (data: any) => {
                this.dataReporte = data;
                this.listData = new MatTableDataSource(data);
                this.listData.sort = this.MatSort;
                this.listData.paginator = this.paginator;
                this.searchBan = false;
              }, ( errorServicio ) => {
                console.log(errorServicio);
              });
          } else {
            this.searchViajesCorporativos(this.clienteId);
          }
        }
      });
  }

  saveLiquidacion() {
    // tslint:disable-next-line:quotemark
    let xmlViajes: string = "<?xml version='1.0' encoding='ISO-8859-1'?><ROOT>";
    for (let i = 0; i < this.selection.selected.length; i++) {
      // tslint:disable-next-line:quotemark
      xmlViajes += "<Viajes ";
      // tslint:disable-next-line:quotemark
      xmlViajes += "ViajeId='" + this.selection.selected[i].viaje + "' />";
    }
    // tslint:disable-next-line:quotemark
    xmlViajes += "</ROOT>";
    const _title: string = 'Registro de Liquidación';
		const _description: string = '¿Estas seguro de realizar la liquidación?';
		const _waitDesciption: string = 'Registrando Liquidación';

		const dialogRef = this.utilsService.confirmLiquidacion(_title, _description, _waitDesciption, this.filterCliente, xmlViajes, this.total, this.usuario, 'E');
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        if (res > 0) {
          const _refreshUrl = 'procesos/liquidacion-empresa/view?id=' + res;
		      this.router.navigateByUrl(_refreshUrl);
        }
      }
    });
  }

  downloadEXCEL() {
    this.sName = `${ this.nroLiquidacion_view }`;
    this.excelFileName = `Liquidacion-${ this.nroLiquidacion_view }.xlsx`;
    const workbook = new Excel.Workbook();
    workbook.creator = 'Web';
    workbook.lastModifiedBy = 'Web';
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.addWorksheet(this.sName, { views: [{ state: 'frozen', ySplit: 0, xSplit: 20, activeCell: 'A1', showGridLines: true }] });
    const sheet = workbook.getWorksheet(1);
    sheet.addRow([`N° lIQUIDACION:`, `${ this.nroLiquidacion_view }`, '', `Conductor:`, `${ this.empresa_view }`]);
    sheet.addRow([`Fecha Liquidación:`, `${ this.datePipe.transform(this.fecha_view, 'dd-MM-yyyy') }`, '', `Monto Total:`, `${ this.total_view }`]);
    sheet.getColumn(4).width = 30;
    sheet.getRow(4).values = this.cols;

    sheet.columns = [
      { key: 'fecha', width: 12 },
      { key: 'horainicio', width: 12 },
      { key: 'horafin', width: 12 },
      { key: 'solicitante', width: 30 },
      { key: 'area', width: 28 },
      { key: 'centrocostos', width: 20 },
      { key: 'origen', width: 30 },
      { key: 'destino', width: 30 },
      { key: 'unidad' },
      { key: 'placa' },
      { key: 'conductor', width: 30 },
      { key: 'montoliquidado' }
    ];

    ['A4', 'B4', 'C4', 'D4', 'E4', 'F4', 'G4', 'H4', 'I4', 'J4', 'K4', 'L4', 'M4'].map(key => {
      sheet.getCell(key).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'EDFC00' }
      };
      });

    sheet.addRows(this.dataReporte);
    workbook.xlsx.writeBuffer().then(dataReporte => {
      const blob = new Blob([dataReporte], { type: this.blobType });
      const url = window.URL.createObjectURL(blob);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.href = url;
      a.download = this.excelFileName;
      a.click();
    });
  }

  updateTarifa(viaje: number) {
    const _title: string = 'Modificar Tarifa';
		const _description: string = '¿Estas seguro de realizar de modificar la tarifa inicial?';
		const _waitDesciption: string = 'Modificando tarifa....';
    if (this.liquidacion_id > 0) {
      const dialogRef = this.utilsService.updateTarifa(_title, _description, _waitDesciption, viaje, this.liquidacion_id, 2);
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          this.total_view = res[0][0]['total'];
          this.liquidacion.getDetalleLiquidacion(this.liquidacion_id).subscribe(
            (data: any) => {
              this.dataReporte = data;
              this.listData = new MatTableDataSource(data);
              this.listData.sort = this.MatSort;
              this.listData.paginator = this.paginator;
              this.searchBan = false;
            }, ( errorServicio ) => {
              console.log(errorServicio);
              this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            });
        }
      });
    } else {
      const dialogRef = this.utilsService.updateTarifa(_title, _description, _waitDesciption, viaje, viaje, 1);
  		dialogRef.afterClosed().subscribe(res => {
  			if (!res[1]) {
  				return;
        } else {
          this.searchViajesCorporativos(this.filterCliente);
          this.selection.clear();
          this.total = 0;
        }
      });
    }
    return;
  }

}
