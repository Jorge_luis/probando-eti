import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy, Pipe, PipeTransform} from '@angular/core';
import { EmpresasService } from '../../../../../../../core/services/empresas.service';
import { LiquidacionService } from '../../../../../../../core/services/liquidacion.service';
import {FormControl, Validators} from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DatePipe } from '@angular/common';
import { SubheaderService } from '../../../../../../../core/services/layout/subheader.service';
import { UtilsService } from '../../../_core/utils/utils.service';


@Component({
  selector: 'm-liquidacion-empresa-list',
  templateUrl: './liquidacion-empresa-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})

export class LiquidacionEmpresaListComponent implements OnInit {
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  searchBan: boolean = false;
  searchKey: string;
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  arrayEmpresas: any[] = [];
  filterEmpresa = new FormControl('', [Validators.required]);
  selectFormControl = new FormControl('', Validators.required);
  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['fechaliquidacion', 'codigoliquidacion', 'dtipodocumento', 'documento', 'total', 'estado', 'actions'];
  filtroEmpresa: number;
  filtroFechaInicio: string;
  filtroFechaFin: string;

  constructor(
    private empresa: EmpresasService,
    private liquidacion: LiquidacionService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private subheaderService: SubheaderService,
    private utilsService: UtilsService,
  ) { }

  ngOnInit() {
    this.subheaderService.setTitle('Liquidación Empresas');
    this.getEmpresas();
    this.listData = new MatTableDataSource(this.array);
    this.listData.sort = this.MatSort;
    this.listData.paginator = this.paginator;
  }

  getEmpresas() {
    this.empresa.getEmpresas(1).subscribe(
      (data: any) => {
          this.arrayEmpresas = data;
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
  }

  loadLiquidacionFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadLiquidacionFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }
  searchLiquidacion() {
    if (this.filterEmpresa.status === 'INVALID') {
      this.toastr.warningToastr('Seleccione una empresa.', 'Alert!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
    } else {
      this.searchBan = true;
      this.filtroEmpresa = this.filterEmpresa.value;
      this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
      this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));
      this.liquidacion.getLiquidacion('0001', this.filtroEmpresa, this.filtroFechaInicio, this.filtroFechaFin).subscribe(
        (data: any) => {
          this.array = data;
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
          this.searchBan = false;
          }, ( errorServicio ) => {
            console.log(errorServicio);
          });
    }
  }

  getLiquidacionCssClassByEstado(status: string = '', observacion: number): string {
		switch (status) {
      case '0001':
        switch (observacion) {
          case 0:
            return 'warning';
          default:
            return 'danger';
        }
      case '0002':
        return 'primary';
      case '0003':
				return 'success';
			case '0004':
				return 'danger';
		}
		return '';
  }

  getLiquidacionEstadoString(status: string = '', observacion: number): string {
		switch (status) {
      case '0001':
        switch (observacion) {
          case 0:
            return 'Pendiente';
          default:
            return 'Con Observaciones';
        }
      case '0002':
        return 'Aprobado';
      case '0003':
				return 'Pagado';
			case '0004':
				return 'Rechazado';
		}
		return '';
  }

  filterLiquidaciones() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  cancelarLiquidacion(Liquidacion: number) {
		const _title: string = 'Realizar Cancelación de Liquidación';
		const _description: string = 'Por favor ingrese el documento de pago.';
		const _waitDesciption: string = 'Registrando pago de liquidación....';

		const dialogRef = this.utilsService.cancelLiquidacion(_title, _description, _waitDesciption, Liquidacion, '1');
		dialogRef.afterClosed().subscribe(res => {
      console.log(res);
			if (!res[1]) {
				return;
			} else {
        console.log(res);
        this.searchLiquidacion();
      }
		});
  }

}
