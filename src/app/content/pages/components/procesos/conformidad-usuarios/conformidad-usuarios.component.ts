import { Component, OnInit, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { SubheaderService } from '../../../../../core/services/layout/subheader.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { BehaviorSubject } from 'rxjs';
import { DatePipe } from '@angular/common';
import { UtilsService } from '../_core/utils/utils.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AprobacionSolicitudService } from '../../../../../core/services/aprobacion-solicitud.service';
import { SelectionModel } from '@angular/cdk/collections';


@Component({
  selector: 'm-conformidad-usuarios',
  templateUrl: './conformidad-usuarios.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class ConformidadUsuariosComponent implements OnInit {

  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['select', 'dniSolicitante', 'nombres', 'area', 'centroCosto', 'fechaSolicitud', 'horaSolicitud', 'dEstado', 'actions'];
  filterFechaIni: Date;
  filterFechaFin: Date;
  maxDateIni: Date = new Date();
  minDateFin: Date = new Date();
  maxDateFin: Date = new Date();
  filtroFechaInicio: string;
  filtroFechaFin: string;
  searchBan: boolean = false;
  array: any[] = [];
  empresaCodigo: string;
  searchKey: string;
  selection = new SelectionModel(true, []);
  usuariosResult: any[] = [];
  usuariosCont: any[] = [];
  filtroEstado: string = '';

  constructor(
    private subheaderService: SubheaderService,
    private datePipe: DatePipe,
    private utilsService: UtilsService,
    private conformidad: AprobacionSolicitudService,
    public toastr: ToastrManager,
  ) { }

  ngOnInit() {
    this.subheaderService.setTitle('Aprobación de Usuarios');
    this.empresaCodigo = localStorage.getItem('cliente');
  }

  loadSolicitudFechaIni() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaFin = null;
        this.minDateFin = this.filterFechaIni;
      } else {
        this.filterFechaIni = this.filterFechaIni;
        this.minDateFin = this.filterFechaIni;
      }
    }
  }

  loadSolicitudFechaFin() {
    if (this.filterFechaIni === undefined ) {
      this.filterFechaIni = null;
    } else {
      if (this.filterFechaIni > this.filterFechaFin) {
        this.filterFechaIni = null;
        this.maxDateIni = this.filterFechaFin;
      }
    }
  }

  searchSolicitudCorporativo() {
    this.selection.clear();
    this.searchBan = true;
    this.filtroFechaInicio = this.filterFechaIni === undefined ? null : (this.datePipe.transform(this.filterFechaIni, 'yyyy-MM-dd'));
    this.filtroFechaFin = this.filterFechaFin === undefined ? null : (this.datePipe.transform(this.filterFechaFin, 'yyyy-MM-dd'));
    // this.filtroEstado = this.filtroEstado === '' ? null : this.filtroEstado;

    console.log(this.filtroEstado);

    this.conformidad.getServiciosEmpresa(this.empresaCodigo, this.filtroFechaInicio, this.filtroFechaFin, this.filtroEstado).subscribe(
      (data: any) => {
        this.array = data;
        this.usuariosResult = data;
        this.listData = new MatTableDataSource(this.array);
        this.listData.sort = this.MatSort;
        this.listData.paginator = this.paginator;
        this.searchBan = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
  }

  getSolicitudCssClassByEstado(status: string = ''): string {
		switch (status) {
			case 'SOLICITADO':
        return 'warning';
      case 'APROBADO':
				return 'success';
			case 'RECHAZADO':
				return 'danger';
		}
		return '';
  }

  getSolicitudEstadoString(status: string = ''): string {
		switch (status) {
			case 'SOLICITADO':
        return 'Pendiente';
      case 'APROBADO':
				return 'Aprobado';
			case 'RECHAZADO':
				return 'Rechazado';
		}
		return '';
  }

  filterUsuarios() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  /** SELECTION */
	isAllSelected() {
    this.usuariosCont = [];
    for (let i = 0; i < this.usuariosResult.length; i++) {
      if (this.usuariosResult[i].estado === '0001') {
        this.usuariosCont.push(this.usuariosResult[i].aprobacionSolicitud);
      }
    }
    if (this.usuariosCont.length > 0) {
      const numSelected = this.selection.selected.length;
      const numRows = this.usuariosResult.length;
      return numSelected === numRows;
    } else {
      const numSelected = this.selection.selected.length;
      // const numRows = this.usuariosResult.length;
      return numSelected === 0;
    }
  }

	 /** Selects all rows if they are not all selected; otherwise clear selection. */
   masterToggle() {
		if (this.isAllSelected()) {
			this.selection.clear();
		} else {
			this.usuariosResult.forEach(lesson => this.selection.select(lesson));
		}
  }

  rechazarUsuario(Solicitud: number, Token: string) {
		const _title: string = 'Rechazar Usuario';
		const _description: string = '¿Estás seguro de rechazar al usuario seleccionado?';
		const _waitDesciption: string = 'Rechazando Usuario';

		const dialogRef = this.utilsService.deleteElement(_title, _description, _waitDesciption, Solicitud, 1, Token);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
			} else {
        this.selection.clear();
        this.searchSolicitudCorporativo();
      }
		});
  }

  aceptarUsuario(Solicitud: number, Token: string) {
		const _title: string = 'Aprobación de Usuario';
		const _description: string = '¿Estás seguro de aprobar el usuario seleccionado?';
		const _waitDesciption: string = 'Usuario en aprobación...';
		const _deleteMessage = `Usuario aprobado correctamente.`;

		const dialogRef = this.utilsService.confirmElement(_title, _description, _waitDesciption, Solicitud, 1, Token);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.searchSolicitudCorporativo();
      }
		});
  }

  rechazarUsuarios() {
		const _title: string = 'Rechazar Usuarios';
		const _description: string = '¿Estás seguro de rechazar los usuarios seleccionados?';
		const _waitDesciption: string = 'Rechazando usuarios';
    const idsForDeletion: any[] = [];
		for (let i = 0; i < this.selection.selected.length; i++) {
      if (this.selection.selected[i].estado !== '0002') {
        const asistencia = new Object();
        asistencia['id'] = this.selection.selected[i].aprobacionSolicitud;
        asistencia['token'] = this.selection.selected[i].tokenFCM;
        idsForDeletion.push(asistencia);
      }
    }

		const dialogRef = this.utilsService.deleteElements(_title, _description, _waitDesciption, idsForDeletion, 2);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      }
      this.selection.clear();
      this.searchSolicitudCorporativo();
		});
  }

  confirmarUsuarios() {
		const _title: string = 'Aprobación de Usuarios';
		const _description: string = '¿Estás seguro de aprobar los usuarios seleccionados?';
		const _waitDesciption: string = 'Aprobando usuarios';
    const idsForDeletion: any[] = [];
		for (let i = 0; i < this.selection.selected.length; i++) {
      if (this.selection.selected[i].estado !== '0003') {
        const asistencia = new Object();
        asistencia['id'] = this.selection.selected[i].aprobacionSolicitud;
        asistencia['token'] = this.selection.selected[i].tokenFCM;
        idsForDeletion.push(asistencia);
      }
    }

		const dialogRef = this.utilsService.confirmElements(_title, _description, _waitDesciption, idsForDeletion, 2);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.selection.clear();
        this.searchSolicitudCorporativo();
      }
		});
  }
}
