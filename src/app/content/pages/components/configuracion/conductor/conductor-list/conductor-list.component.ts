import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { EmpresasService } from '../../../../../../core/services/empresas.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { UtilsServiceConductor } from '../_core/utils/utils.service';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DatePipe } from '@angular/common';
import { ConductorService } from '../../../../../../core/services/conductor.service';


@Component({
  selector: 'm-conductor-list',
  templateUrl: './conductor-list.component.html',
  styleUrls: ['./conductor-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class ConductorListComponent implements OnInit {
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  @ViewChild('searchInput') searchInput: ElementRef;
  titulo: string;
  descripcion: string;
  esperaDescripcion: string;
  searchBan: boolean;
  constructor(
    private conductor: ConductorService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private subheaderService: SubheaderService,
    private utilsService: UtilsServiceConductor,
  ) { }

  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['dni', 'nombrecompleto', 'nrolicencia', 'direccion', 'telefono', 'activo', 'actions'];

  ngOnInit() {
		this.getConductores();
  }

  getConductores() {
    this.searchBan = true;
    this.conductor.getConductores(null).subscribe(
      (data: any) => {
          this.searchBan = false;
          this.array = data;
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.array = [];
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
          this.toastr.errorToastr('Se produjo un error al listar las liquidaciones, por favor refresque la pantalla.', 'Error!', {
            toastTimeout: 3000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }



  searchConductor() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  getConductorEstadoString(status: boolean = false): string {
		switch (status) {
			case false:
				return 'Inactivo';
			case true:
				return 'Activo';
		}
		return '';
  }

  getConductorCssClassByEstado(status: boolean = false): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
		return '';
  }

  activarConductor(Conductor: number, Activo: boolean) {
    if (Activo) {
      this.titulo = 'Desactivar Conductor';
      this.descripcion = '¿Esta seguro de desactivar al conductor seleccionado?';
      this.esperaDescripcion = 'Desactivando conductor...';
    } else {
      this.titulo = 'Activar Conductor';
      this.descripcion = '¿Esta seguro de activar nuevamente al conductor seleccionado?';
      this.esperaDescripcion = 'Activando conductor...';
    }
		const _title: string = this.titulo;
		const _description: string = this.descripcion;
		const _waitDesciption: string = this.esperaDescripcion;

		const dialogRef = this.utilsService.activeConductor(_title, _description, _waitDesciption, Conductor, Activo);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.getConductores();
      }
		});
  }

}
