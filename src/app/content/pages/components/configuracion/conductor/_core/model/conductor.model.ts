export class ConductorModel  {
    conductor: number;
    nombres: string;
    apellidos: string;
    dni: string;
    telefono: string;
    activo: boolean;
    direccion: string;
    gradoAcademico: string;
    fechaNacimiento: Date;
    nroLicencia: string;
    inicioVigencia: Date;
    finVigencia: Date;
    clase: string;
    categoria: string;
    copiaDni: boolean;
    antecedentesPenales: boolean;
    informePsicologico: boolean;
    usuarioRegistro: string;
    imgenPerfil: string;

	clear() {
		this.nombres = null;
		this.apellidos = null;
        this.dni = null;
        this.telefono = null,
        this.activo = true;
        this.direccion = null,
        this.gradoAcademico = null,
        this.fechaNacimiento = null,
        this.nroLicencia = null,
        this.inicioVigencia = null,
        this.finVigencia = null,
        this.clase = null,
        this.categoria = null,
        this.copiaDni = false;
        this.imgenPerfil = null;
        this.antecedentesPenales = false;
        this.informePsicologico = false;
	}
}