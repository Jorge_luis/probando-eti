import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActiveConductorDialogComponent } from '../../_shared/active-conductor-dialog/active-conductor-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class UtilsServiceConductor {

  constructor(
    private dialog: MatDialog
	) {}

	activeConductor(title: string = '', description: string = '', waitDesciption: string = '', conductor: number, estado: boolean) {
		return this.dialog.open(ActiveConductorDialogComponent, {
			data: { title, description, waitDesciption,  conductor, estado},
			width: '440px'
		});
  }

}
