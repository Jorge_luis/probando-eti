import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConductorModel } from '../_core/model/conductor.model';
import { ConductorService } from '../../../../../../core/services/conductor.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'm-conductor-edit',
  templateUrl: './conductor-edit.component.html',
  styleUrls: ['./conductor-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class ConductorEditComponent implements OnInit {
  conductor: ConductorModel;
  oldConductor: ConductorModel;
  usuario: string;
  conductor_id: number;
  srcResult: string;
  formConductor: FormGroup;
  isSave: boolean;
  isUpdate: boolean;
  images: any = [];
  allfiles: any = [];
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();

  constructor(
    private subheaderService: SubheaderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private serviceConductor: ConductorService,
    private _formBuilder: FormBuilder,
    public toastr: ToastrManager,
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
    this.activatedRoute.queryParams.subscribe(params => {
      const id = +params.id;
      this.conductor_id = id;
			if (id && id > 0) {
        this.isUpdate = true;
        const newConductor = new ConductorModel();
        newConductor.clear();
        this.conductor = newConductor;
        this.oldConductor = Object.assign({}, newConductor);
        this.subheaderService.setTitle('Detalles de Conductor');
        this.initEmpresa(id);
//  {nombres: null, apellidos: null, dni: null, telefono: null, activo: true}
        this.serviceConductor.getDatosEmpresa(id).subscribe(res => {
          this.conductor = res;
          this.oldConductor = Object.assign({}, res);
          const controls = this.formConductor.controls;

          controls['nombres'].setValue(this.conductor[0].nombres);
          controls['apellidos'].setValue(this.conductor[0].apellidos);
          controls['dni'].setValue(this.conductor[0].dni);
          controls['activo'].setValue(this.conductor[0].activo);
          controls['telefono'].setValue(this.conductor[0].telefono);
          controls['direccion'].setValue(this.conductor[0].direccion);
          controls['gradoAcademico'].setValue(this.conductor[0].gradoacademico);
          controls['fechaNacimiento'].setValue(this.conductor[0].fechanacimiento);
          controls['nroLicencia'].setValue(this.conductor[0].nrolicencia);
          controls['inicioVigencia'].setValue(this.conductor[0].iniciovigencia);
          controls['finVigencia'].setValue(this.conductor[0].finvigencia);
          controls['clase'].setValue(this.conductor[0].clase);
          controls['categoria'].setValue(this.conductor[0].categoria);
          controls['copiaDni'].setValue(this.conductor[0].copiadni);
          controls['antecedentesPenales'].setValue(this.conductor[0].antecedentepenales);
          controls['informePsicologico'].setValue(this.conductor[0].infopsicologico);
          controls['imgenPerfil'].setValue(this.conductor[0].foto);
        });
      } else {
        this.isUpdate = false;
        const newConductor = new ConductorModel();
        newConductor.clear();
        this.conductor = newConductor;
        this.oldConductor = Object.assign({}, newConductor);

        this.initEmpresa(id);
        this.subheaderService.setTitle('Nuevo Conductor');
			}
    });
  }


  getComponentTitle() {
		let result = 'Nuevo Conductor';
		if (!this.conductor_id) {
			return result;
		}

		result = `Detalle de Conductor`;
		return result;
  }

  initEmpresa(id: number) {
    this.createFormConductor();
    if (id > 0) {
  		this.subheaderService.setBreadcrumbs([
        { title: 'Configuración', page: '/configuracion/conductor' },
        { title: 'Conductor',  page: '/configuracion/conductor' },
        { title: 'Detalle Conductor', page: '/configuracion/conductor/view', queryParams: { id: id } }
      ]);
      return;
    } else {
  		this.subheaderService.setBreadcrumbs([
        { title: 'Configuración', page: '/configuracion/conductor' },
        { title: 'Conductor',  page: '/configuracion/conductor' },
        { title: 'Nuevo Conductor', page: '/configuracion/conductor/add' }
      ]);
      return;
    }
  }

  createFormConductor() {
		this.formConductor = this._formBuilder.group({
			nombres: [this.conductor.nombres, Validators.required],
			apellidos: [this.conductor.apellidos, Validators.required],
			dni: [this.conductor.dni, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(8), Validators.maxLength(8)]],
			telefono: [this.conductor.telefono, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
			activo: [this.conductor.activo],
      direccion: [this.conductor.direccion], //
      gradoAcademico: [this.conductor.gradoAcademico], //
      fechaNacimiento: [this.conductor.fechaNacimiento], //
      nroLicencia: [this.conductor.nroLicencia, Validators.required],
      inicioVigencia: [this.conductor.inicioVigencia, Validators.required],
      finVigencia: [this.conductor.finVigencia, Validators.required],
      clase: [this.conductor.clase, Validators.required],
      categoria: [this.conductor.categoria, Validators.required],
      copiaDni: [this.conductor.copiaDni],
      antecedentesPenales: [this.conductor.antecedentesPenales],
      informePsicologico: [this.conductor.informePsicologico],
      imgenPerfil: [this.conductor.imgenPerfil] //
		});
  }

  prepareConductor(): ConductorModel {
		const controls = this.formConductor.controls;
    const _conductor = new ConductorModel();
		_conductor.conductor = this.conductor_id;
		_conductor.nombres = controls['nombres'].value;
		_conductor.apellidos = controls['apellidos'].value;
		_conductor.dni = controls['dni'].value;
		_conductor.telefono = controls['telefono'].value;
    _conductor.activo = controls['activo'].value;
    _conductor.direccion = controls['direccion'].value;
    _conductor.gradoAcademico = controls['gradoAcademico'].value;
    _conductor.fechaNacimiento = controls['fechaNacimiento'].value;
    _conductor.nroLicencia = controls['nroLicencia'].value;
    _conductor.inicioVigencia = controls['inicioVigencia'].value;
    _conductor.finVigencia = controls['finVigencia'].value;
    _conductor.clase = controls['clase'].value;
    _conductor.categoria = controls['categoria'].value;
    _conductor.copiaDni = controls['copiaDni'].value;
    _conductor.antecedentesPenales = controls['antecedentesPenales'].value;
    _conductor.informePsicologico = controls['informePsicologico'].value;
    _conductor.imgenPerfil = controls['imgenPerfil'].value;
    _conductor.usuarioRegistro = this.usuario;
		return _conductor;
  }

  resetConductor() {
    this.conductor = Object.assign({}, this.oldConductor);
    this.createFormConductor();
    this.formConductor.markAsPristine();
    this.formConductor.markAsUntouched();
    this.formConductor.updateValueAndValidity();
    this.conductor_id = null;
  }

  onSumbitNew() {
    const controls = this.formConductor.controls;
		if (this.formConductor.invalid) {
			Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.toastr.warningToastr('Ingrese los campos obligatorios.', 'Advertencia!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
			return;
    }

    const editedConductor = this.prepareConductor();
    console.log(editedConductor);
		if (editedConductor.conductor > 0) {
			this.updateConductor(editedConductor);
		} else {
			this.createConductor(editedConductor);
    }
  }

  createConductor(_conductor: ConductorModel) {
    this.isSave = true;
    this.serviceConductor.createConductor(_conductor).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.conductor_id = Number(res);
        this.toastr.successToastr('Se registro correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
          this.isSave = false;
        if (Number(res) === -1) {
          this.toastr.warningToastr('El DNI ingresado ya existe.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          this.toastr.errorToastr('Se produjo un error al registrar.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        }
      }
    }, ( errorServicio ) => {
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  updateConductor(_conductor: ConductorModel) {
    this.isSave = true;
    this.serviceConductor.updateConductor(_conductor).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.toastr.successToastr('Se modificaron los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.isSave = false;
        this.toastr.errorToastr('Se produjo un error al modificar.', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.isSave = false;
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  // fileuploads(event) {
  //   this.images = [];
  //   const file = event.target.files;
  //   console.log(file);
  //   if (file) {
  //     for (let i = 0; i < file.length; i++) {
  //       const image = {
  //         name : '',
  //         url : ''
  //       };
  //       this.allfiles.push(file[i]);
  //       image.name = file[i].name;
  //       const reader = new FileReader();
  //       reader.onload = (filedata) => {
  //         image.url = reader.result + '';
  //         this.images.push(image);
  //       };
  //       reader.readAsDataURL(file[i]);
  //     }
  //   }    
  // }

  goBack(id = 0) {
		let _backUrl = 'configuracion/conductor';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
  }

}

