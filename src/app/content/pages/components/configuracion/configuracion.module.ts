import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ConfiguracionComponent } from '././configuracion.component';
import { EmpresasComponent } from './empresas/empresas.component';
import { ConductorComponent } from './conductor/conductor.component';
import { PropietariosComponent } from './propietarios/propietarios.component';
import { VehiculoComponent } from './vehiculo/vehiculo.component';
import { EmpresasListComponent } from './empresas/empresas-list/empresas-list.component';
import {MatStepperModule} from '@angular/material/stepper';
import { MaterialFileInputModule } from 'ngx-material-file-input';

import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule,
	DateAdapter
} from '@angular/material';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CodePreviewModule } from '../../../partials/content/general/code-preview/code-preview.module';
import { PartialsModule } from '../../../partials/partials.module';
import { CoreModule } from '../../../../core/core.module';
import { MaterialPreviewModule } from '../../../partials/content/general/material-preview/material-preivew.module';
import { NgbAlertConfig } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { EmpresasEditComponent } from './empresas/empresas-edit/empresas-edit.component';
import { AreasComponent } from './empresas/_subs/areas/areas.component';
import { AprobadoresComponent } from './empresas/_subs/aprobadores/aprobadores.component';
import { ActiveAreaDialogComponent } from './empresas/_shared/active-area-dialog/active-area-dialog.component';
import { UtilsService } from './empresas/_core/utils/utils.service';
import { UtilsServiceConductor } from './conductor/_core/utils/utils.service';
import { UtilsServicePropiertario } from './propietarios/_core/utils/utils.service';
import { ActiveAprobadorDialogComponent } from './empresas/_shared/active-aprobador-dialog/active-aprobador-dialog.component';
import { ActiveClienteDialogComponent } from './empresas/_shared/active-cliente-dialog/active-cliente-dialog.component';
import { ActiveContactoDialogComponent } from './empresas/_shared/active-contacto-dialog/active-contacto-dialog.component';
import { ConductorListComponent } from './conductor/conductor-list/conductor-list.component';
import { ConductorEditComponent } from './conductor/conductor-edit/conductor-edit.component';
import { ActiveConductorDialogComponent } from './conductor/_shared/active-conductor-dialog/active-conductor-dialog.component';
import { PropietarioListComponent } from './propietarios/propietario-list/propietario-list.component';
import { PropietarioEditComponent } from './propietarios/propietario-edit/propietario-edit.component';
import { ActivePropietarioDialogComponent } from './propietarios/_shared/active-propietario-dialog/active-propietario-dialog.component';

const routes: Routes = [
	{
		path: '',
		component: ConfiguracionComponent,
		children: [
			{
				path: 'empresa',
				component: EmpresasListComponent
			},
			{
				path: 'empresa/add',
				component: EmpresasEditComponent
			},
			{
				path: 'empresa/view',
				component: EmpresasEditComponent
			},
			{
				path: 'empresa/view/:id',
				component: EmpresasEditComponent
			},

            {
				path: 'conductor',
				component: ConductorListComponent
			},
			{
				path: 'conductor/add',
				component: ConductorEditComponent
			},
			{
				path: 'conductor/view',
				component: ConductorEditComponent
			},
			{
				path: 'conductor/view/:id',
				component: ConductorEditComponent
			},

			{
				path: 'propietario',
				component: PropietarioListComponent
			},
			{
				path: 'propietario/add',
				component: PropietarioEditComponent
			},
			{
				path: 'propietario/view',
				component: PropietarioEditComponent
			},
			{
				path: 'propietario/view/:id',
				component: PropietarioEditComponent
			},
			{
				path: 'vehiculo',
				component: VehiculoComponent
			},
		]
	}
];

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	// suppressScrollX: true
};

@NgModule({
	imports: [
		CommonModule,
		PartialsModule,
		NgbModule,
		CodePreviewModule,
		CoreModule,
		MaterialPreviewModule,
		RouterModule.forChild(routes),
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		PerfectScrollbarModule,
		MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
		MatProgressSpinnerModule,
		MatIconModule,
		MatSelectModule,
		MatMenuModule,
		MatProgressBarModule,
		MatButtonModule,
		MatCheckboxModule,
		MatDialogModule,
		MatTabsModule,
		MatNativeDateModule,
		MatCardModule,
		MatRadioModule,
		MatDatepickerModule,
		MatAutocompleteModule,
		MatSnackBarModule,
		MatTooltipModule,
		AngularFontAwesomeModule,
		MatStepperModule,
		MaterialFileInputModule
	],
	exports: [RouterModule],
	declarations: [
        ConfiguracionComponent,
        EmpresasComponent,
        ConductorComponent,
        VehiculoComponent,
        PropietariosComponent,
        EmpresasListComponent,
        EmpresasEditComponent,
        AreasComponent,
        AprobadoresComponent,
        ActiveAreaDialogComponent,
        ActiveAprobadorDialogComponent,
        ActiveClienteDialogComponent,
        ActiveContactoDialogComponent,
        ConductorListComponent,
        ConductorEditComponent,
        ActiveConductorDialogComponent,
        PropietarioListComponent,
        PropietarioEditComponent,
        ActivePropietarioDialogComponent,
	],
	providers: [
		NgbAlertConfig, {
		provide: PERFECT_SCROLLBAR_CONFIG,
		useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
	  },
	  UtilsService,
	  UtilsServiceConductor,
	  UtilsServicePropiertario
	],
	entryComponents: [
		ActiveAreaDialogComponent,
		ActiveAprobadorDialogComponent,
		ActiveClienteDialogComponent,
		ActiveContactoDialogComponent,
		ActiveConductorDialogComponent,
		ActivePropietarioDialogComponent
	]
})
export class ConfiguracionModule {
	constructor(private dateAdapter: DateAdapter<Date>) {
		dateAdapter.setLocale('en-in'); // DD/MM/YYYY
	  }
}
