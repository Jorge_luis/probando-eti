import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { EmpresasService } from '../../../../../../core/services/empresas.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { UtilsService } from '../_core/utils/utils.service';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';

@Component({
  selector: 'm-empresas-list',
  templateUrl: './empresas-list.component.html',
  styleUrls: ['./empresas-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmpresasListComponent implements OnInit {
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  @ViewChild('searchInput') searchInput: ElementRef;
  titulo: string;
  descripcion: string;
  esperaDescripcion: string;
  searchBan: boolean;
  constructor(
    private empresa: EmpresasService,
    private utilsService: UtilsService,
    private subheaderService: SubheaderService,
  ) { }

  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['razonSocial', 'ruc', 'dominio', 'direccion', 'activo', 'actions'];

  ngOnInit() {
    this.subheaderService.setTitle('Empresas');
		this.getEmpresas();
  }

  getEmpresas() {
    this.empresa.getEmpresas(null).subscribe(
      (data: any) => {
          this.array = data;
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
        }, ( errorServicio ) => {
          console.log(errorServicio);
        });
  }

  searchEmpresas() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  getEmpresaEstadoString(status: boolean = false): string {
		switch (status) {
			case false:
				return 'Inactivo';
			case true:
				return 'Activo';
		}
		return '';
  }

  getEmpresaCssClassByEstado(status: boolean = false): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
		return '';
  }

  activarCliente(Cliente: number, Activo: boolean) {
    if (Activo) {
      this.titulo = 'Desactivar Cliente';
      this.descripcion = '¿Esta seguro de desactivar el cliente seleccionado?';
      this.esperaDescripcion = 'Desactivando cliente...';
    } else {
      this.titulo = 'Activar Cliente';
      this.descripcion = '¿Esta seguro de activar nuevamente el cliente seleccionado?';
      this.esperaDescripcion = 'Activando cliente...';
    }
		const _title: string = this.titulo;
		const _description: string = this.descripcion;
		const _waitDesciption: string = this.esperaDescripcion;

		const dialogRef = this.utilsService.activeCliente(_title, _description, _waitDesciption, Cliente, Activo);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.getEmpresas();
      }
		});
  }

}
