import { Component, OnInit, Input, AfterViewInit, ElementRef, ViewChild, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { AprobadorModel } from '../../_core/models/aprobador.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilsService } from '../../_core/utils/utils.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { EmpresasService } from '../../../../../../../core/services/empresas.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'm-aprobadores',
  templateUrl: './aprobadores.component.html',
  styleUrls: ['./aprobadores.component.scss']
})
export class AprobadoresComponent implements OnInit, OnChanges  {
  aprobador: AprobadorModel;
  oldAprobador: AprobadorModel;
  isSave: Boolean;
  usuario: string;
  formAprobadores: FormGroup;
  searchBanAprobadores: boolean = true;
  titulo: string;
  descripcion: string;
  esperaDescripcion: string;
  hide = true;
  @Input() arrayAprobadores: any[];
  @Input() area_id: string;
  @Input() nombre_area: string;
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  displayedColumnsApro: string[] = ['nombrecompleto', 'usuario', 'dni', 'principal', 'activo', 'actions'];
  listDataAprobadores: MatTableDataSource<any>;

  constructor(
    private _formBuilder: FormBuilder,
    private utilsService: UtilsService,
    public toastr: ToastrManager,
    private serviceEmpresa: EmpresasService,
  ) {
  }

  ngOnInit() {
    this.isSave = false;
    this.usuario = localStorage.getItem('usuario');
    const newAprobador = new AprobadorModel();
    newAprobador.clear();
    this.aprobador = newAprobador;
    this.oldAprobador = Object.assign({}, newAprobador);
    this.initAprobadores();
  }

  initAprobadores() {
    this.createFormAprobadores();
  }

  /* DISEÑO DE ESTADOS GRILLAS */
  getEstadoString(status: boolean = false): string {
		switch (status) {
			case false:
				return 'Inactivo';
			case true:
				return 'Activo';
		}
		return '';
  }

  getEstadoCssClassByEstado(status: boolean = false): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
		return '';
  }

  createFormAprobadores() {
		this.formAprobadores = this._formBuilder.group({
      aprobadorNombres: [this.aprobador.nombres, Validators.required],
			aprobadorApellidos: [this.aprobador.apellidos, Validators.required],
			aprobadorDni: [this.aprobador.dni, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(8), Validators.maxLength(8)]],
			aprobadorCorreo: [this.aprobador.correo, Validators.email],
			aprobadorUsuario: [this.aprobador.usuario, Validators.required],
      aprobadorContrasena: [this.aprobador.contrasena, {disabled: true}],
      aprobadorPrincipal: [this.aprobador.principal],
      aprobadorActivo: [this.aprobador.activo]
		});
  }

  resetAprobadores() {
    this.formAprobadores.controls['aprobadorContrasena'].enable();
		this.aprobador = Object.assign({}, this.oldAprobador);
    this.createFormAprobadores();
		this.formAprobadores.markAsPristine();
    this.formAprobadores.markAsUntouched();
    this.formAprobadores.updateValueAndValidity();
  }

  updateDatosAprobador(Aprobador: number) {
    this.formAprobadores.controls['aprobadorContrasena'].disable();
    const datosAprobador = this.arrayAprobadores.find(aprobador => aprobador.aprobador === Aprobador);
    this.aprobador = datosAprobador;
    this.createFormAprobadores();
  }

  ngOnChanges(changes: SimpleChanges) {
    const newAprobador = new AprobadorModel();
    newAprobador.clear();
    this.aprobador = newAprobador;
    this.oldAprobador = Object.assign({}, newAprobador);
    this.initAprobadores();
    this.searchBanAprobadores = true;
    this.listDataAprobadores = new MatTableDataSource(this.arrayAprobadores);
    this.listDataAprobadores.sort = this.MatSort;
    this.listDataAprobadores.paginator = this.paginator;
    this.searchBanAprobadores = false;
  }

  aprobadorCliente(area: number) {
    this.searchBanAprobadores = true;
    this.serviceEmpresa.getAprobadoresClientes(area).subscribe(
      (data: any) => {
        this.arrayAprobadores = data;
        this.listDataAprobadores = new MatTableDataSource(this.arrayAprobadores);
        this.listDataAprobadores.sort = this.MatSort;
        this.listDataAprobadores.paginator = this.paginator;
        this.searchBanAprobadores = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  /*REGISTRO Y EDICION DE AREAS CLIENTE*/
  prepareAprobador(): AprobadorModel {
		const controls = this.formAprobadores.controls;
    const _aprobador = new AprobadorModel();
    _aprobador.aprobador = this.aprobador.aprobador;
    _aprobador.area = Number(this.area_id);
    _aprobador.nombres = controls['aprobadorNombres'].value;
		_aprobador.apellidos = controls['aprobadorApellidos'].value;
		_aprobador.dni = controls['aprobadorDni'].value;
		_aprobador.usuario = controls['aprobadorUsuario'].value;
		_aprobador.contrasena = controls['aprobadorContrasena'].value;
		_aprobador.correo = controls['aprobadorCorreo'].value;
		_aprobador.activo = controls['aprobadorActivo'].value;
		_aprobador.principal = controls['aprobadorPrincipal'].value;
    _aprobador.usuarioRegistro = this.usuario;
		return _aprobador;
  }

  submitAprobador() {
    const controls = this.formAprobadores.controls;
		if (this.formAprobadores.invalid) {
			Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.toastr.warningToastr('Ingrese los campos obligatorios.', 'Advertencia!', {
        toastTimeout: 3000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
			return;
    }
    const editedAprobador = this.prepareAprobador();
    if (editedAprobador.area > 0) {
      if (editedAprobador.aprobador > 0) {
        this.updateAprobador(editedAprobador);
      } else {
        if (editedAprobador.contrasena === '') {
          this.toastr.infoToastr('Debe Asignarle una contraseña al usuario.', 'Información!', {
            toastTimeout: 3000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          this.createAprobador(editedAprobador);
        }
      }
    } else {
      this.toastr.warningToastr('Selececcione una área.', 'Advertencia!', {
        toastTimeout: 3000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
    }
  }

  createAprobador(_aprobador: AprobadorModel) {
    this.isSave = true;
    this.serviceEmpresa.createAprobadores(_aprobador).subscribe(res => {
      if (Number(res) > 0) {
        this.toastr.successToastr('Se registro los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        this.isSave = false;
        this.aprobadorCliente(Number(this.area_id));
        this.resetAprobadores();
      } else {
        if (Number(res) === -1) {
          this.isSave = false;
          this.toastr.warningToastr('Ya existe un aprobador registrado en la empresa, con el DNI ingresado.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.isSave = false;
            this.toastr.warningToastr('Ya existe un aprobador como principal.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            if (Number(res) === -3) {
              this.isSave = false;
              this.toastr.warningToastr('El usuario ya existe por favor asiganar otro usuario.', 'Advertencia!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            } else {
              this.isSave = false;
              this.toastr.errorToastr('Se produjo un error al registrar.', 'Error!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            }
          }
        }
      }
    }, ( errorServicio ) => {
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  updateAprobador(_aprobador: AprobadorModel) {
    this.isSave = true;
    this.serviceEmpresa.updateAprobadores(_aprobador).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.aprobadorCliente(Number(this.area_id));
        this.resetAprobadores();
        this.toastr.successToastr('Se modificaron los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        if (Number(res) === -1) {
          this.isSave = false;
          this.toastr.warningToastr('Ya existe un aprobador registrado en la empresa, con el DNI ingresado.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.isSave = false;
            this.toastr.warningToastr('Ya existe un aprobador como principal.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            if (Number(res) === -3) {
              this.isSave = false;
              this.toastr.warningToastr('El usuario ya existe por favor asiganar otro usuario.', 'Advertencia!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            } else {
              this.isSave = false;
              this.toastr.errorToastr('Se produjo un error al registrar.', 'Error!', {
                toastTimeout: 2000,
                showCloseButton: true,
                animate: 'fade',
                progressBar: true
              });
            }
          }
        }
      }
    }, ( errorServicio ) => {
      this.isSave = false;
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }


  activarAprobador(Aprobador: number, Activo: boolean) {
    if (Activo) {
      this.titulo = 'Desactivar Área';
      this.descripcion = '¿Esta seguro de desactivar al aprobador seleccionado?';
      this.esperaDescripcion = 'Desactivando aprobador...';
    } else {
      this.titulo = 'Activar Área';
      this.descripcion = '¿Esta seguro de activar nuevamente el aprobador seleccionado?';
      this.esperaDescripcion = 'Activando aprobador...';
    }
		const _title: string = this.titulo;
		const _description: string = this.descripcion;
		const _waitDesciption: string = this.esperaDescripcion;

		const dialogRef = this.utilsService.activeAprobador(_title, _description, _waitDesciption, Aprobador, Activo);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.aprobadorCliente(Number(this.area_id));
      }
		});
  }


}
