import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { MatStepper, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { EmpresasService } from '../../../../../../../core/services/empresas.service';
import { Areamodel } from '../../_core/models/area.model';
import { UtilsService } from '../../_core/utils/utils.service';

@Component({
  selector: 'm-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {
  area: Areamodel;
  oldArea: Areamodel;

  isSave: Boolean;
  usuario: string;
  formAreas: FormGroup;

  titulo: string;
  descripcion: string;
  esperaDescripcion: string;
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;

  @Input() empresa_id: number;
  @Output() public arrayAprobadores = new EventEmitter();
  @Output() public area_id = new EventEmitter();
  @Output() public nombre_area = new EventEmitter();

  searchBanArea: boolean = false;
  arrayArea: any[] = [];
  arrayAprobador: any[] = [];
  listDataArea: MatTableDataSource<any>;
  displayedColumnsArea: string[] = ['nombre', 'descripcion', 'centroCosto', 'activo', 'actions'];

  constructor(
    private serviceEmpresa: EmpresasService,
    public toastr: ToastrManager,
    private _formBuilder: FormBuilder,
    private utilsService: UtilsService,
  ) { }

  ngOnInit() {
    this.isSave = false;
    this.usuario = localStorage.getItem('usuario');
    if (this.empresa_id && this.empresa_id > 0) {
      const newArea = new Areamodel();
      newArea.clear();
      this.area = newArea;
      this.oldArea = Object.assign({}, newArea);
      this.initArea(this.empresa_id);
      this.areasCliente(this.empresa_id);
    } else {
      const newArea = new Areamodel();
      newArea.clear();
      this.area = newArea;
      this.oldArea = Object.assign({}, newArea);
      this.initArea(this.empresa_id);
    }
  }

  initArea(id: number) {
    this.createFormArea();
  }

  createFormArea() {
		this.formAreas = this._formBuilder.group({
			areaNombre: [this.area.nombre, Validators.required],
			areaDescripcion: [this.area.descripcion],
			areaCentroCosto: [this.area.centroCosto, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
			areaActivo: [this.area.activo]
		});
  }

   /* DISEÑO DE ESTADOS GRILLAS */
   getEstadoString(status: boolean = false): string {
		switch (status) {
			case false:
				return 'Inactivo';
			case true:
				return 'Activo';
		}
		return '';
  }

  getEstadoCssClassByEstado(status: boolean = false): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
		return '';
  }

  /*REGISTRO Y EDICION DE AREAS CLIENTE*/
  areasCliente(cliente: number) {
    this.searchBanArea = true;
    this.serviceEmpresa.getAreaClientes(cliente).subscribe(
      (data: any) => {
        this.arrayArea = data;
        this.listDataArea = new MatTableDataSource(this.arrayArea);
        this.listDataArea.sort = this.MatSort;
        this.listDataArea.paginator = this.paginator;
        this.searchBanArea = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  updateDatosArea(Area: number) {
    const datosArea = this.arrayArea.find(area => area.area === Area);
    this.area = datosArea;
    this.createFormArea();
  }

  prepareArea(): Areamodel {
		const controls = this.formAreas.controls;
    const _area = new Areamodel();
    _area.cliente = this.empresa_id;
    _area.area = this.area.area;
    _area.nombre = controls['areaNombre'].value;
		_area.descripcion = controls['areaDescripcion'].value;
		_area.centroCosto = controls['areaCentroCosto'].value;
		_area.activo = controls['areaActivo'].value;
    _area.usuarioRegistro = this.usuario;
		return _area;
  }

  submitArea() {
    const controls = this.formAreas.controls;
		if (this.formAreas.invalid) {
			Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.toastr.warningToastr('Ingrese los campos obligatorios.', 'Advertencia!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
			return;
    }
    const editedArea = this.prepareArea();
    console.log(editedArea);
    if (editedArea.area > 0) {
      console.log('Editar');
			this.updateArea(editedArea);
		} else {
      console.log('Nuevo');
			this.createArea(editedArea);
    }

  }

  createArea(_area: Areamodel) {
    this.isSave = true;
    this.serviceEmpresa.createArea(_area).subscribe(res => {
      if (Number(res) > 0) {
        this.toastr.successToastr('Se registro los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        this.isSave = false;
        this.areasCliente(this.empresa_id);
        this.resetArea();
      } else {
        if (Number(res) === -1) {
          this.isSave = false;
          this.toastr.warningToastr('Ya existe un área con el centro de costos ingresado.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.isSave = false;
            this.toastr.warningToastr('Ya existe una área con ese nombre.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.isSave = false;
            this.toastr.errorToastr('Se produjo un error al registrar.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
      }
    }, ( errorServicio ) => {
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  updateArea(_area: Areamodel) {
    this.isSave = true;
    this.serviceEmpresa.updateArea(_area).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.areasCliente(this.empresa_id);
        this.resetArea();
        this.toastr.successToastr('Se modificaron los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        if (Number(res) === -1) {
          this.isSave = false;
          this.toastr.warningToastr('Ya existe un área con el centro de costos ingresado.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.isSave = false;
            this.toastr.warningToastr('Ya existe una área con ese nombre.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.isSave = false;
            this.toastr.errorToastr('Se produjo un error al modificar.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
      }
    }, ( errorServicio ) => {
      this.isSave = false;
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  verAprobadores(area: number, nombre_area: string): void {
    this.serviceEmpresa.getAprobadoresClientes(area).subscribe(
      (data: any) => {
          if (data.length > 0) {
            this.arrayAprobador = data;
            this.arrayAprobadores.emit(this.arrayAprobador);
            console.log(area);
            this.area_id.emit(area);
            this.nombre_area.emit(nombre_area);
          } else {
            this.arrayAprobador = data;
            this.arrayAprobadores.emit(this.arrayAprobador);
            console.log(area);
            this.area_id.emit(area);
            this.nombre_area.emit(nombre_area);
            this.toastr.infoToastr('No se encontraron aprobadores registrados.', 'Información!', {
              toastTimeout: 4000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });

  }

  activarArea(Area: number, Activo: boolean) {
    if (Activo) {
      this.titulo = 'Desactivar Área';
      this.descripcion = '¿Esta seguro de desactivar el área seleccionada?';
      this.esperaDescripcion = 'Desactivando área...';
    } else {
      this.titulo = 'Activar Área';
      this.descripcion = '¿Esta seguro de activar nuevamente el área seleccionada?';
      this.esperaDescripcion = 'Activando área...';
    }
		const _title: string = this.titulo;
		const _description: string = this.descripcion;
		const _waitDesciption: string = this.esperaDescripcion;

		const dialogRef = this.utilsService.activeArea(_title, _description, _waitDesciption, Area, Activo);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.areasCliente(this.empresa_id);
      }
		});
  }

  resetArea() {
		this.area = Object.assign({}, this.oldArea);
    this.createFormArea();
		this.formAreas.markAsPristine();
    this.formAreas.markAsUntouched();
    this.formAreas.updateValueAndValidity();
  }


}
