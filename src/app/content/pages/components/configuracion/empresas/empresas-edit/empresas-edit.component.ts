import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { BehaviorSubject } from 'rxjs';
import { MatStepper, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmpresaModel } from '../_core/models/empresa.model';
import { ContactoModel } from '../_core/models/contacto.model';
import { Areamodel } from '../_core/models/area.model';
import { EmpresasService } from '../../../../../../core/services/empresas.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { UtilsService } from '../_core/utils/utils.service';

@Component({
  selector: 'm-empresas-edit',
  templateUrl: './empresas-edit.component.html',
  styleUrls: ['./empresas-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class EmpresasEditComponent implements OnInit {
  empresa: EmpresaModel;
  oldEmpresa: EmpresaModel;
  contacto: ContactoModel;
  oldContacto: ContactoModel;
  area: Areamodel;
  oldArea: Areamodel;
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('searchInput') searchInput: ElementRef;
  @ViewChild('myStep') myStep;
  @ViewChild('stepper') stepper: MatStepper;
  isLinear = false;
  formEmpresa: FormGroup;
  formContacto: FormGroup;
  formAprobadores: FormGroup;
  formAreas: FormGroup;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();
  empresa_id: number;
  usuario: string;
  isSave: boolean;
  isUpdate: boolean;
  isNewEmpresa: boolean;
  hide = true;
  displayedColumns: string[] = ['nombreCompleto', 'dni', 'email', 'telefono', 'principal', 'activo', 'actions'];
  displayedColumnsApro: string[] = ['nombresApro', 'dni', 'principal', 'actions'];
  displayedColumnsArea: string[] = ['nombre', 'descripcion', 'centroCosto', 'actions'];
  step1Completed: boolean;
  titulo: string;
  descripcion: string;
  esperaDescripcion: string;
  searchBanContacto: boolean = false;
  arrayContacto: any[] = [];
  listDataContacto: MatTableDataSource<any>;
  area_id: number;
  arrayAprobadores: any;
  nombre_area: string;
  constructor(
    private subheaderService: SubheaderService,
    private activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private router: Router,
    private serviceEmpresa: EmpresasService,
    public toastr: ToastrManager,
    private utilsService: UtilsService,
  ) { }

  ngOnInit() {
    this.isSave = false;
    this.step1Completed = false;
    this.usuario = localStorage.getItem('usuario');
    this.activatedRoute.queryParams.subscribe(params => {
      const id = +params.id;
      this.empresa_id = id;
			if (id && id > 0) {
        this.step1Completed = true;
        this.isUpdate = true;
        this.isNewEmpresa = true;
        this.subheaderService.setTitle('Detalles de Empresa');

        const newEmpresa = new EmpresaModel();
        newEmpresa.clear();
        this.empresa = newEmpresa;
        this.oldEmpresa = Object.assign({}, newEmpresa);

        const newContacto = new ContactoModel();
        newContacto.clear();
        this.contacto = newContacto;
        this.oldContacto = Object.assign({}, newContacto);

        const newArea = new Areamodel();
        newArea.clear();
        this.area = newArea;
        this.oldArea = Object.assign({}, newArea);

        this.initEmpresa(id);

        this.serviceEmpresa.getDatosEmpresa(id).subscribe(res => {
          this.empresa = res;
          this.oldEmpresa = Object.assign({}, res);
          const controls = this.formEmpresa.controls;
          controls['razonSocial'].setValue(this.empresa[0].razonSocial);
          controls['ruc'].setValue(this.empresa[0].ruc);
          controls['direccion'].setValue(this.empresa[0].direccion);
          controls['dominio'].setValue(this.empresa[0].dominio);
          controls['linkImagen'].setValue(this.empresa[0].linkImagen);
          controls['rubro'].setValue(this.empresa[0].rubro);
          controls['activo'].setValue(this.empresa[0].activo);
        });

        this.contactosCliente(id);
			} else {
        this.isUpdate = false;
        this.isNewEmpresa = false;
        const newEmpresa = new EmpresaModel();
				newEmpresa.clear();
        this.empresa = newEmpresa;
				this.oldEmpresa = Object.assign({}, newEmpresa);

        const newContacto = new ContactoModel();
        newContacto.clear();
        this.contacto = newContacto;
        this.oldContacto = Object.assign({}, newContacto);

        const newArea = new Areamodel();
        newArea.clear();
        this.area = newArea;
        this.oldArea = Object.assign({}, newArea);

        this.initEmpresa(id);
        this.subheaderService.setTitle('Nueva Empresa');
			}
    });
  }

  getComponentTitle() {
		let result = 'Nueva Empresa';
		if (!this.empresa_id) {
			return result;
		}

		result = `Detalle de Empresa`;
		return result;
  }

  initEmpresa(id: number) {
    this.createFormEmpresa();
    this.createFormContacto();
    this.createFormAprobadores();
    if (id > 0) {
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Configuración', page: '/configuracion/empresa' },
        { title: 'Empresas',  page: '/configuracion/empresa' },
        { title: 'Detalle Empresa', page: '/configuracion/empresa/view', queryParams: { id: id } }
      ]);
      return;
    } else {
  		this.loadingSubject.next(false);
  		this.subheaderService.setBreadcrumbs([
        { title: 'Configuración', page: '/configuracion/empresa' },
        { title: 'Empresas',  page: '/configuracion/empresa' },
        { title: 'Nueva Empresa', page: '/configuracion/empresa/add' }
      ]);
      return;
    }
  }

  /* CREACION DE FORMULARIOS */
  createFormEmpresa() {
		this.formEmpresa = this._formBuilder.group({
			razonSocial: [this.empresa.razonSocial, Validators.required],
			ruc: [this.empresa.ruc, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(11), Validators.maxLength(11)]],
			direccion: [this.empresa.direccion],
			rubro: [this.empresa.rubro],
			dominio: [this.empresa.dominio],
			linkImagen: [this.empresa.linkImagen],
			activo: [this.empresa.activo]
		});
  }

  createFormContacto() {
		this.formContacto = this._formBuilder.group({
			contactoNombres: [this.contacto.nombres, Validators.required],
			contactoApellidos: [this.contacto.apellidos, Validators.required],
			contactoEmail: [this.contacto.email, Validators.email],
			contactoTelefono: [this.contacto.telefono, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
			contactoPrincipal: [this.contacto.principal],
      contactoActivo: [this.contacto.activo],
      contactoDni: [this.contacto.dni, [Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(8), Validators.maxLength(8)]]
		});
  }

  createFormArea() {
		this.formAreas = this._formBuilder.group({
			areaNombre: [this.area.nombre, Validators.required],
			areaDescripcion: [this.area.descripcion],
			areaCentroCosto: [this.area.centroCosto],
			areaActivo: [this.area.activo]
		});
  }

  createFormAprobadores() {
		this.formAprobadores = this._formBuilder.group({
      aprobadorNombres: ['', Validators.required],
			aprobadorApellidos: ['', Validators.required],
			aprobadorDni: ['', [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(8), Validators.maxLength(8)]],
			aprobadorUsuario: ['', Validators.required],
      aprobadorContrasena: ['', Validators.required],
      aprobadorPrincipal: [''],
		});
  }

  /* RESETEAR FORMULARIOS */
  resetEmpresa() {
    this.empresa = Object.assign({}, this.oldEmpresa);
    this.createFormEmpresa();
    this.formEmpresa.markAsPristine();
    this.formEmpresa.markAsUntouched();
    this.formEmpresa.updateValueAndValidity();
  }

  resetContacto() {
		this.contacto = Object.assign({}, this.oldContacto);
    this.createFormContacto();
		this.formContacto.markAsPristine();
    this.formContacto.markAsUntouched();
    this.formContacto.updateValueAndValidity();
  }

  /* DISEÑO DE ESTADOS GRILLAS */
  getEstadoString(status: boolean = false): string {
		switch (status) {
			case false:
				return 'Inactivo';
			case true:
				return 'Activo';
		}
		return '';
  }

  getEstadoCssClassByEstado(status: boolean = false): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
		return '';
	}

  /*REGISTRO Y EDICION DE DATOS CLIENTE*/
  prepareEmpresa(): EmpresaModel {
		const controls = this.formEmpresa.controls;
    const _empresa = new EmpresaModel();
		_empresa.cliente = this.empresa_id;
		_empresa.razonSocial = controls['razonSocial'].value;
		_empresa.ruc = controls['ruc'].value;
		_empresa.direccion = controls['direccion'].value;
		_empresa.dominio = controls['dominio'].value;
    _empresa.linkImagen = controls['linkImagen'].value;
    _empresa.rubro = controls['rubro'].value;
    _empresa.activo = controls['activo'].value;
    _empresa.usuarioRegistro = this.usuario;
		return _empresa;
  }

  onSubmit() {
		const controls = this.formEmpresa.controls;
		if (this.formEmpresa.invalid) {
			Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.toastr.warningToastr('Ingrese los campos obligatorios.', 'Advertencia!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
			return;
    }

    this.step1Completed = true;
    const editedEmpresa = this.prepareEmpresa();
		if (editedEmpresa.cliente > 0) {
			this.updateEmpresa(editedEmpresa);
		} else {
			this.createEmpresa(editedEmpresa);
		}
  }

  createEmpresa(_empresa: EmpresaModel) {
    this.isSave = true;
    this.serviceEmpresa.createEmpresa(_empresa).subscribe(res => {
      if (Number(res) > 0) {
        this.isUpdate = true;
        this.isSave = false;
        this.stepper.next();
        this.empresa_id = Number(res);
        this.toastr.successToastr('Se registro correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
          this.isSave = false;
          this.step1Completed = false;
        if (Number(res) === -1) {
          this.toastr.warningToastr('El RUC ingresado ya existe.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.toastr.warningToastr('El Dominio ingresado ya existe.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.toastr.errorToastr('Se produjo un error al registrar.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
      }
    }, ( errorServicio ) => {
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  updateEmpresa(_empresa: EmpresaModel) {
    this.isSave = true;
    this.serviceEmpresa.updateEmpresa(_empresa).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.stepper.next();
        this.toastr.successToastr('Se modificaron los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.isSave = false;
        this.step1Completed = false;
        if (Number(res) === -1) {
          this.toastr.warningToastr('El RUC ingresado ya existe.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.toastr.warningToastr('El Dominio ingresado ya existe.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.toastr.errorToastr('Se produjo un error al modificar.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
      }
    }, ( errorServicio ) => {
      this.isSave = false;
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  /*REGISTRO Y EDICION DE DATOS CONTACTOS CLIENTES*/
  contactosCliente(cliente: number) {
    this.searchBanContacto = true;
    this.serviceEmpresa.getContactosCliente(cliente).subscribe(
      (data: any) => {
        this.arrayContacto = data;
        this.listDataContacto = new MatTableDataSource(this.arrayContacto);
        this.listDataContacto.sort = this.MatSort;
        this.listDataContacto.paginator = this.paginator;
        this.searchBanContacto = false;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.toastr.errorToastr('Se produjo un error al cargar al listar detalle de servicios.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }

  prepareContacto(): ContactoModel {
		const controls = this.formContacto.controls;
    const _contacto = new ContactoModel();
    _contacto.cliente = this.empresa_id;
    _contacto.contactoCliente = this.contacto.contactoCliente;
		_contacto.nombres = controls['contactoNombres'].value;
		_contacto.apellidos = controls['contactoApellidos'].value;
		_contacto.email = controls['contactoEmail'].value;
		_contacto.telefono = controls['contactoTelefono'].value;
    _contacto.activo = controls['contactoActivo'].value;
    _contacto.principal = controls['contactoPrincipal'].value;
    _contacto.dni = controls['contactoDni'].value;
    _contacto.usuarioRegistro = this.usuario;
		return _contacto;
  }

  updateDatosContacto(Contacto: number) {
    const datosContacto = this.arrayContacto.find(contacto => contacto.contactoCliente === Contacto);
    this.contacto = datosContacto;
    this.createFormContacto();
  }

  submitContacto() {
    const controls = this.formContacto.controls;
		if (this.formContacto.invalid) {
			Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.toastr.warningToastr('Ingrese los campos obligatorios.', 'Advertencia!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
			return;
    }

    const editedContacto = this.prepareContacto();
    if (editedContacto.contactoCliente > 0) {
      console.log('Editar');
			this.updateContacto(editedContacto);
		} else {
      console.log('Nuevo');
			this.createContacto(editedContacto);
    }
  }

  createContacto(_contacto: ContactoModel) {
    this.isSave = true;
    this.serviceEmpresa.createContacto(_contacto).subscribe(res => {
      if (Number(res) > 0) {
        this.toastr.successToastr('Se registro los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
        this.isSave = false;
        this.contactosCliente(this.empresa_id);
        this.resetContacto();
      } else {
        if (Number(res) === -1) {
          this.isSave = false;
          this.toastr.warningToastr('Ya existe un contacto registrado como principal.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.isSave = false;
            this.toastr.warningToastr('El contacto ya existe.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.isSave = false;
            this.toastr.errorToastr('Se produjo un error al registrar.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
      }
    }, ( errorServicio ) => {
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  updateContacto(_contacto: ContactoModel) {
    this.isSave = true;
    this.serviceEmpresa.updateContacto(_contacto).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.contactosCliente(this.empresa_id);
        this.resetContacto();
        this.toastr.successToastr('Se modificaron los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        if (Number(res) === -1) {
          this.isSave = false;
          this.toastr.warningToastr('Ya existe un contacto registrado como principal.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          if (Number(res) === -2) {
            this.isSave = false;
            this.toastr.warningToastr('El contacto ya existe.', 'Advertencia!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          } else {
            this.isSave = false;
            this.toastr.errorToastr('Se produjo un error al modificar.', 'Error!', {
              toastTimeout: 2000,
              showCloseButton: true,
              animate: 'fade',
              progressBar: true
            });
          }
        }
      }
    }, ( errorServicio ) => {
      this.isSave = false;
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }


  activarContacto(Contacto: number, Activo: boolean) {
    if (Activo) {
      this.titulo = 'Desactivar contacto';
      this.descripcion = '¿Esta seguro de desactivar el contacto seleccionado?';
      this.esperaDescripcion = 'Desactivando contacto...';
    } else {
      this.titulo = 'Activar contacto';
      this.descripcion = '¿Esta seguro de activar nuevamente el contacto seleccionado?';
      this.esperaDescripcion = 'Activando contacto...';
    }
		const _title: string = this.titulo;
		const _description: string = this.descripcion;
		const _waitDesciption: string = this.esperaDescripcion;

		const dialogRef = this.utilsService.activeContacto(_title, _description, _waitDesciption, Contacto, Activo);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.contactosCliente(this.empresa_id);
      }
		});
  }

  siguiente() {
    this.stepper.next();
  }

  goBack(id = 0) {
		let _backUrl = 'configuracion/empresa';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
  }

}
