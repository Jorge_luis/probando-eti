import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { EmpresasService } from '../../../../../../../core/services/empresas.service';

@Component({
  selector: 'm-active-aprobador-dialog',
  templateUrl: './active-aprobador-dialog.component.html',
  styleUrls: ['./active-aprobador-dialog.component.scss']
})
export class ActiveAprobadorDialogComponent implements OnInit {
  viewLoading: boolean = false;
  usuario: string;

  constructor(
    private serviceEmpresa: EmpresasService,
    public toastr: ToastrManager,
    public dialogRef: MatDialogRef<ActiveAprobadorDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
  }

  onNoClick(): void {
		this.dialogRef.close();
  }

  onYesClick() {
    this.viewLoading = true;
    if (this.data.estado) {
      this.serviceEmpresa.putActivarAprobador(this.data.aprobador, !this.data.estado, this.usuario).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se desactivo correctamente el aprobador seleccionado.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al desactivar aprobador..', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al desactivar aprobador..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
			});
    } else {
      this.serviceEmpresa.putActivarAprobador(this.data.aprobador, !this.data.estado, this.usuario).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se activo correctamente el aprobador seleccionado.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al activar aprobador..', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al activar aprobador..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
			});
    }
  }

}
