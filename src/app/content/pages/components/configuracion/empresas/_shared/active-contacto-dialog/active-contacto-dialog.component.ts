import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { EmpresasService } from '../../../../../../../core/services/empresas.service';

@Component({
  selector: 'm-active-contacto-dialog',
  templateUrl: './active-contacto-dialog.component.html',
  styleUrls: ['./active-contacto-dialog.component.scss']
})
export class ActiveContactoDialogComponent implements OnInit {
  viewLoading: boolean = false;
	usuario: string;
  constructor(
    private serviceEmpresa: EmpresasService,
    public toastr: ToastrManager,
    public dialogRef: MatDialogRef<ActiveContactoDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
		this.usuario = localStorage.getItem('usuario');
  }

  onNoClick(): void {
		this.dialogRef.close();
  }

  onYesClick() {
    this.viewLoading = true;
    if (this.data.estado) {
      this.serviceEmpresa.putActivarContacto(this.data.contacto, !this.data.estado, this.usuario).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se desactivo correctamente el contacto seleccionado.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al desactivar contacto..', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al desactivar contacto..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
			});
    } else {
      this.serviceEmpresa.putActivarContacto(this.data.contacto, !this.data.estado, this.usuario).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se activo correctamente el contacto seleccionado.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al activar contacto..', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al activar contacto..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
			});
    }
  }

}
