export class ContactoModel  {
    contactoCliente: number;
    cliente: number;
	nombres: string;
	apellidos: string;
    email: string;
    dni: string;
    telefono: string;
    usuarioRegistro: string;
    principal: boolean;
    activo: boolean;

	clear() {
		this.nombres = null;
		this.apellidos = null;
		this.email = null;
		this.dni = null;
		this.telefono = '';
        this.principal = false;
        this.activo = true;
	}
}