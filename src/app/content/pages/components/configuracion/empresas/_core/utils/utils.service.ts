
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActiveAreaDialogComponent } from '../../_shared/active-area-dialog/active-area-dialog.component';
import { ActiveAprobadorDialogComponent } from '../../_shared/active-aprobador-dialog/active-aprobador-dialog.component';
import { ActiveClienteDialogComponent } from '../../_shared/active-cliente-dialog/active-cliente-dialog.component';
import { ActiveContactoDialogComponent } from '../../_shared/active-contacto-dialog/active-contacto-dialog.component';



@Injectable()
export class UtilsService {
  constructor(
    private dialog: MatDialog
	) {}

	activeCliente(title: string = '', description: string = '', waitDesciption: string = '', cliente: number, estado: boolean) {
		return this.dialog.open(ActiveClienteDialogComponent, {
			data: { title, description, waitDesciption,  cliente, estado},
			width: '440px'
		});
	}

	activeContacto(title: string = '', description: string = '', waitDesciption: string = '', contacto: number, estado: boolean) {
		return this.dialog.open(ActiveContactoDialogComponent, {
			data: { title, description, waitDesciption,  contacto, estado},
			width: '440px'
		});
	}

	activeArea(title: string = '', description: string = '', waitDesciption: string = '', area: number, estado: boolean) {
		return this.dialog.open(ActiveAreaDialogComponent, {
			data: { title, description, waitDesciption,  area, estado},
			width: '440px'
		});
	}

	activeAprobador(title: string = '', description: string = '', waitDesciption: string = '', aprobador: number, estado: boolean) {
		return this.dialog.open(ActiveAprobadorDialogComponent, {
			data: { title, description, waitDesciption,  aprobador, estado},
			width: '440px'
		});
	}

}
