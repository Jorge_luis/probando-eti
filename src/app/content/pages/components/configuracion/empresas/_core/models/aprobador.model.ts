export class AprobadorModel  {
    aprobador: number;
    area: number;
	nombres: string;
	apellidos: string;
    usuario: string;
    contrasena: string;
    correo: string;
    dni: string;
    activo: boolean;
    principal: boolean;
    usuarioRegistro: string;

	clear() {
		this.nombres = null;
		this.apellidos = null;
        this.dni = null;
        this.usuario = null,
        this.contrasena = null,
        this.correo = null,
        this.usuarioRegistro = null,
        this.activo = true;
        this.principal = false;
	}
}