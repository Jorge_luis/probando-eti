export class EmpresaModel  {
	cliente: number;
	razonSocial: string;
	ruc: string;
    direccion: string;
    rubro: string;
	dominio: string;
    linkImagen: string;
    usuarioRegistro: string;
    activo: boolean;

	clear() {
		this.razonSocial = '';
		this.ruc = '';
		this.direccion = '';
		this.rubro = '';
		this.dominio = '';
        this.linkImagen = '';
        this.activo = true;
	}
}