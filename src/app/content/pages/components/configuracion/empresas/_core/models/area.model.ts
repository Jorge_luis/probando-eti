export class Areamodel  {
    area: number;
    cliente: number;
	nombre: string;
	descripcion: string;
    centroCosto: string;
    usuarioRegistro: string;
    activo: boolean;

	clear() {
		this.nombre = null;
		this.descripcion = null;
		this.centroCosto = null;
        this.activo = true;
	}
}