import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivePropietarioDialogComponent } from '../../_shared/active-propietario-dialog/active-propietario-dialog.component';



@Injectable({
  providedIn: 'root'
})
export class UtilsServicePropiertario {

  constructor(
    private dialog: MatDialog
	) {}

	activePropietario(title: string = '', description: string = '', waitDesciption: string = '', propietario: number, estado: boolean) {
		return this.dialog.open(ActivePropietarioDialogComponent, {
			data: { title, description, waitDesciption,  propietario, estado},
			width: '440px'
		});
  }
  
}
