export class PropietarioModel  {
    propietario: number;
	tipoDoc: string;
	nroDocumento: string;
	nombres: string;
	apellidos: string;
    telefono1: string;
    telefono2: string;
    correo: string;
    direccion: string;
    gradoAcademico: string;
    observacion: string;
    activo: boolean;
    copiaDni: boolean;
    fechaNacimiento: Date;
    usuarioRegistro: string;

	clear() {
		this.tipoDoc = null;
		this.nroDocumento = null;
        this.nombres = null;
        this.apellidos = null,
        this.telefono1 = null,
        this.telefono2 = null,
        this.correo = null,
        this.direccion = null,
        this.observacion = null,
        this.gradoAcademico = null,
        this.activo = true,
        this.copiaDni = false;
	}
}