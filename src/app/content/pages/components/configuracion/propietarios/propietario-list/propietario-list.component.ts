import { Component, OnInit, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { EmpresasService } from '../../../../../../core/services/empresas.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { UtilsServicePropiertario } from '../_core/utils/utils.service';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { DatePipe } from '@angular/common';
import { PropietarioService } from '../../../../../../core/services/propietario.service';

@Component({
  selector: 'm-propietario-list',
  templateUrl: './propietario-list.component.html',
  styleUrls: ['./propietario-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})
export class PropietarioListComponent implements OnInit {
  @ViewChild(MatSort) MatSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  @ViewChild('searchInput') searchInput: ElementRef;
  titulo: string;
  descripcion: string;
  esperaDescripcion: string;
  searchBan: boolean;
  constructor(
    private propierarioService: PropietarioService,
    public toastr: ToastrManager,
    private datePipe: DatePipe,
    private subheaderService: SubheaderService,
    private utilsService: UtilsServicePropiertario,
  ) { }

  array: any[] = [];
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['documento', 'nombrecompleto', 'telefono', 'direccion', 'correo', 'activo', 'actions'];

  ngOnInit() {
		this.getPropietarios();
  }

  getPropietarios() {
    this.searchBan = true;
    this.propierarioService.getPropietarios(null).subscribe(
      (data: any) => {
          this.searchBan = false;
          this.array = data;
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
        }, ( errorServicio ) => {
          console.log(errorServicio);
          this.array = [];
          this.listData = new MatTableDataSource(this.array);
          this.listData.sort = this.MatSort;
          this.listData.paginator = this.paginator;
          this.toastr.errorToastr('Se produjo un error al listar las liquidaciones, por favor refresque la pantalla.', 'Error!', {
            toastTimeout: 3000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        });
  }



  searchPropietario() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  getPropietarioEstadoString(status: boolean = false): string {
		switch (status) {
			case false:
				return 'Inactivo';
			case true:
				return 'Activo';
		}
		return '';
  }

  getPropietarioCssClassByEstado(status: boolean = false): string {
		switch (status) {
			case false:
				return 'danger';
			case true:
				return 'success';
		}
		return '';
  }

  activarPropietario(Propietario: number, Activo: boolean) {
    if (Activo) {
      this.titulo = 'Desactivar Propietario';
      this.descripcion = '¿Esta seguro de desactivar al Propietario seleccionado?';
      this.esperaDescripcion = 'Desactivando Propietario...';
    } else {
      this.titulo = 'Activar Propietario';
      this.descripcion = '¿Esta seguro de activar nuevamente al Propietario seleccionado?';
      this.esperaDescripcion = 'Activando Propietario...';
    }
		const _title: string = this.titulo;
		const _description: string = this.descripcion;
		const _waitDesciption: string = this.esperaDescripcion;

		const dialogRef = this.utilsService.activePropietario(_title, _description, _waitDesciption, Propietario, Activo);
		dialogRef.afterClosed().subscribe(res => {
			if (!res) {
				return;
      } else {
        this.getPropietarios();
      }
		});
  }

}

