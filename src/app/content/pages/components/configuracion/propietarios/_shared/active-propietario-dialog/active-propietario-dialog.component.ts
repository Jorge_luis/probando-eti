import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrManager } from 'ng6-toastr-notifications';
import { PropietarioService } from '../../../../../../../core/services/propietario.service';

@Component({
  selector: 'm-active-propietario-dialog',
  templateUrl: './active-propietario-dialog.component.html',
  styleUrls: ['./active-propietario-dialog.component.scss']
})
export class ActivePropietarioDialogComponent implements OnInit {
  viewLoading: boolean = false;
  usuario: string;

  constructor(
    private servicePropietario: PropietarioService,
    public toastr: ToastrManager,
    public dialogRef: MatDialogRef<ActivePropietarioDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
  }

  onNoClick(): void {
		this.dialogRef.close();
  }

  onYesClick() {
    this.viewLoading = true;
    if (this.data.estado) {
      this.servicePropietario.putActivarPropietario(this.data.propietario, !this.data.estado, this.usuario).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se desactivo correctamente el propietario seleccionado.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al desactivar propietario..', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al desactivar aprobador..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
			});
    } else {
      this.servicePropietario.putActivarPropietario(this.data.propietario, !this.data.estado, this.usuario).subscribe((data: any) => {
				if (data === 1) {
					this.dialogRef.close(true);
					this.toastr.successToastr('Se activo correctamente el propietario seleccionado.', 'Exito!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				} else {
					this.dialogRef.close(true);
					this.toastr.errorToastr('Se produjo un error al activar propietario..', 'Error!', {
						toastTimeout: 2000,
						showCloseButton: true,
						animate: 'fade',
						progressBar: true
					});
				}
			}, ( errorServicio ) => {
				this.dialogRef.close(true);
				this.toastr.errorToastr('Se produjo un error al activar aprobador..', 'Error!', {
					toastTimeout: 2000,
					showCloseButton: true,
					animate: 'fade',
					progressBar: true
				});
				console.log(errorServicio);
			});
    }
  }

}
