import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { SubheaderService } from '../../../../../../core/services/layout/subheader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';
import { BehaviorSubject } from 'rxjs';
import { PropietarioModel } from '../_core/model/propietario.model';
import { PropietarioService } from '../../../../../../core/services/propietario.service';


@Component({
  selector: 'm-propietario-edit',
  templateUrl: './propietario-edit.component.html',
  styleUrls: ['./propietario-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DatePipe]
})

export class PropietarioEditComponent implements OnInit {
  propietario: PropietarioModel;
  oldPropietario: PropietarioModel;
  usuario: string;
  propietario_id: number;
  srcResult: string;
  formPropietario: FormGroup;
  isSave: boolean;
  isUpdate: boolean;
  images: any = [];
  allfiles: any = [];
  maximo: number;
  loadingSubject = new BehaviorSubject<boolean>(false);
  loading$ = this.loadingSubject.asObservable();

  constructor(
    private propietarioService: PropietarioService,
    private subheaderService: SubheaderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _formBuilder: FormBuilder,
    public toastr: ToastrManager,
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem('usuario');
    this.activatedRoute.queryParams.subscribe(params => {
      const id = +params.id;
      this.propietario_id = id;
			if (id && id > 0) {
        this.isUpdate = true;
        this.subheaderService.setTitle('Detalles de propietario');

        const newPropietario = new PropietarioModel();
        newPropietario.clear();
        this.propietario = newPropietario;
        this.oldPropietario = Object.assign({}, newPropietario);

        this.initPropietario(id);

        this.propietarioService.getDatosPropietario(id).subscribe(res => {
          this.propietario = res;
          this.oldPropietario = Object.assign({}, res);
          const controls = this.formPropietario.controls;

          controls['nombres'].setValue(this.propietario[0].nombres);
          controls['apellidos'].setValue(this.propietario[0].apellidos);
          controls['direccion'].setValue(this.propietario[0].direccion);
          controls['tipoDoc'].setValue(this.propietario[0].tipoDoc);
          controls['nroDocumento'].setValue(this.propietario[0].nroDoc);
          controls['telefono1'].setValue(this.propietario[0].telefono1);
          controls['telefono2'].setValue(this.propietario[0].telefono2);
          controls['correo'].setValue(this.propietario[0].correo);
          controls['activo'].setValue(this.propietario[0].activo);
          controls['gradoAcademico'].setValue(this.propietario[0].profesion);
          controls['fechaNacimiento'].setValue(this.propietario[0].fechaNacimiento);
          controls['copiaDni'].setValue(this.propietario[0].copiaDNI);
          controls['observacion'].setValue(this.propietario[0].observacion);
        });

      } else {
        this.isUpdate = false;
        this.subheaderService.setTitle('Nuevo propietario');

        const newPropietario = new PropietarioModel();
        newPropietario.clear();
        this.propietario = newPropietario;
        this.oldPropietario = Object.assign({}, newPropietario);

        this.initPropietario(id);
			}
    });
  }


  getComponentTitle() {
		let result = 'Nuevo Propietario';
		if (!this.propietario_id) {
			return result;
		}

		result = `Detalle de Propietario`;
		return result;
  }

  initPropietario(id: number) {
    this.createFormPropietario();
    if (id > 0) {
  		this.subheaderService.setBreadcrumbs([
        { title: 'Configuración', page: '/configuracion/propietario' },
        { title: 'Propietario',  page: '/configuracion/propietario' },
        { title: 'Detalle Propietario', page: '/configuracion/propietario/view', queryParams: { id: id } }
      ]);
      return;
    } else {
  		this.subheaderService.setBreadcrumbs([
        { title: 'Configuración', page: '/configuracion/propietario' },
        { title: 'Propietario',  page: '/configuracion/propietario' },
        { title: 'Nuevo Propietario', page: '/configuracion/propietario/add' }
      ]);
      return;
    }
  }

  onChangeTipoDoc(event) {
    if (event.value === 'DNI') {
      this.maximo = 8;
    } else {
      this.maximo = 11;
    }
  }

  createFormPropietario() {
		this.formPropietario = this._formBuilder.group({
			nombres: [this.propietario.nombres, Validators.required],
			apellidos: [this.propietario.apellidos, Validators.required],
			direccion: [this.propietario.direccion],
      tipoDoc: [this.propietario.tipoDoc, Validators.required],
      nroDocumento: [this.propietario.nroDocumento, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.minLength(this.maximo), Validators.maxLength(this.maximo)]],
      telefono1: [this.propietario.telefono1, [Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      telefono2: [this.propietario.telefono2, Validators.pattern(/^-?(0|[1-9]\d*)?$/)],
      correo: [this.propietario.correo, Validators.email],
      activo: [this.propietario.activo],
			gradoAcademico: [this.propietario.gradoAcademico],
      fechaNacimiento: [this.propietario.fechaNacimiento],
      copiaDni: [this.propietario.copiaDni],
      observacion: [this.propietario.observacion]
		});
  }

  resetPropietario() {
    this.propietario = Object.assign({}, this.oldPropietario);
    this.createFormPropietario();
    this.formPropietario.markAsPristine();
    this.formPropietario.markAsUntouched();
    this.formPropietario.updateValueAndValidity();
    this.propietario_id = null;
  }

  goBack(id = 0) {
		let _backUrl = 'configuracion/propietario';
		if (id > 0) {
			_backUrl += '?id=' + id;
		}
		this.router.navigateByUrl(_backUrl);
  }

  preparePropietario(): PropietarioModel {
		const controls = this.formPropietario.controls;
    const _propietario = new PropietarioModel();
		_propietario.propietario = this.propietario_id;
		_propietario.nombres = controls['nombres'].value;
		_propietario.apellidos = controls['apellidos'].value;
		_propietario.tipoDoc = controls['tipoDoc'].value;
		_propietario.correo = controls['correo'].value;
		_propietario.nroDocumento = controls['nroDocumento'].value;
    _propietario.activo = controls['activo'].value;
    _propietario.direccion = controls['direccion'].value;
    _propietario.telefono1 = controls['telefono1'].value;
    _propietario.telefono2 = controls['telefono2'].value;
    _propietario.gradoAcademico = controls['gradoAcademico'].value;
    _propietario.fechaNacimiento = controls['fechaNacimiento'].value;
    _propietario.copiaDni = controls['copiaDni'].value;
    _propietario.observacion = controls['observacion'].value;
    _propietario.usuarioRegistro = this.usuario;
		return _propietario;
  }

  onSumbitNew() {
    const controls = this.formPropietario.controls;
		if (this.formPropietario.invalid) {
			Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      this.toastr.warningToastr('Ingrese los campos obligatorios.', 'Advertencia!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
			return;
    } 
    const editedPropietario = this.preparePropietario();
		if (editedPropietario.propietario > 0) {
			this.updatePropietario(editedPropietario);
		} else {
      if (controls['tipoDoc'].value === 'DNI') {
        if (controls['nroDocumento'].value.length !== 8) {
          this.toastr.warningToastr('El documento debe tener ' + this.maximo + '.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
          return;
        }
      } else {
        if (controls['nroDocumento'].value.length !== 11) {
          this.toastr.warningToastr('El documento debe tener ' + this.maximo + '.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
          return;
        }
      }
			this.createPropietario(editedPropietario);
    }
  }

  createPropietario(_propietario: PropietarioModel) {
    this.isSave = true;
    this.propietarioService.createPropietario(_propietario).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.propietario_id = Number(res);
        this.toastr.successToastr('Se registro correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
          this.isSave = false;
        if (Number(res) === -1) {
          this.toastr.warningToastr('El DNI ingresado ya existe.', 'Advertencia!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        } else {
          this.toastr.errorToastr('Se produjo un error al registrar.', 'Error!', {
            toastTimeout: 2000,
            showCloseButton: true,
            animate: 'fade',
            progressBar: true
          });
        }
      }
    }, ( errorServicio ) => {
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

  updatePropietario(_propietario: PropietarioModel) {
    this.isSave = true;
    this.propietarioService.updatePropietario(_propietario).subscribe(res => {
      if (Number(res) > 0) {
        this.isSave = false;
        this.toastr.successToastr('Se modificaron los datos correctamente.', 'Exito!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      } else {
        this.isSave = false;
        this.toastr.errorToastr('Se produjo un error al modificar.', 'Error!', {
          toastTimeout: 2000,
          showCloseButton: true,
          animate: 'fade',
          progressBar: true
        });
      }
    }, ( errorServicio ) => {
      this.isSave = false;
      this.toastr.errorToastr('Se produjo un error al asignar servicio.', 'Error!', {
        toastTimeout: 2000,
        showCloseButton: true,
        animate: 'fade',
        progressBar: true
      });
      console.log(errorServicio);
    });
  }

}

