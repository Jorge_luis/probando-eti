import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmServiceDialogComponent } from './confirm-service-dialog.component';

describe('ConfirmServiceDialogComponent', () => {
  let component: ConfirmServiceDialogComponent;
  let fixture: ComponentFixture<ConfirmServiceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmServiceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmServiceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
