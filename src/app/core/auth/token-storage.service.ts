import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable()
export class TokenStorage {
	/**
	 * Get access token
	 * @returns {Observable<string>}
	 */
	public getAccessToken(): Observable<string> {
		const token: string = <string>localStorage.getItem('accessToken');
		return of(token);
	}

	/**
	 * Get refresh token
	 * @returns {Observable<string>}
	 */
	public getRefreshToken(): Observable<string> {
		const token: string = <string>localStorage.getItem('refreshToken');
		return of(token);
	}

	/**
	 * Get user roles in JSON string
	 * @returns {Observable<any>}
	 */
	public getUserRoles(): Observable<any> {
		const roles: any = localStorage.getItem('userRoles');
		try {
			return of(JSON.parse(roles));
		} catch (e) {}
	}
	
	/**
	 * Set access token
	 * @returns {TokenStorage}
	 */
	public setAccessRol(rol: string): TokenStorage {
		localStorage.setItem('rol', rol);

		return this;
	}

	/**
	 * Set access token
	 * @returns {TokenStorage}
	 */
	public setAccessEmpresa(cliente: string): TokenStorage {
		localStorage.setItem('cliente', cliente);

		return this;
	}

	public setAccessUsuario(usuario: string): TokenStorage {
		localStorage.setItem('usuario', usuario);
		return this;
	}

	public setAccessAprobador(aprobador: string): TokenStorage {
		localStorage.setItem('aprobador', aprobador);
		return this;
	}

	/**
	 * Set access token
	 * @returns {TokenStorage}
	 */
	public setAccessNombres(nombres: string): TokenStorage {
		localStorage.setItem('nombres', nombres);

		return this;
	}

	/**
	 * Set access token
	 * @returns {TokenStorage}
	 */
	public setAccessEmail(correo: string): TokenStorage {
		localStorage.setItem('correo', correo);

		return this;
	}

	/**
	 * Set access token
	 * @returns {TokenStorage}
	 */
	public setAccessCuentaUsuario(cuentaUsuario: string): TokenStorage {
		localStorage.setItem('cuentaUsuario', cuentaUsuario);

		return this;
	}

	/**
	 * Set access token
	 * @returns {TokenStorage}
	 */
	public setAccessToken(token: string): TokenStorage {
		localStorage.setItem('accessToken', token);

		return this;
	}

	/**
	 * Set refresh token
	 * @returns {TokenStorage}
	 */
	public setRefreshToken(token: string): TokenStorage {
		localStorage.setItem('refreshToken', token);

		return this;
	}

	/**
	 * Set user roles
	 * @param roles
	 * @returns {TokenStorage}
	 */
	public setUserRoles(roles: any): any {
		console.log(JSON.parse(roles));
		console.log(JSON.stringify(roles));
		if (roles != null) {
			localStorage.setItem('userRoles', JSON.stringify(JSON.parse(roles)));
		}

		return this;
	}

	/**
	 * Remove tokens
	 */
	public clear() {
		localStorage.removeItem('accessToken');
		localStorage.removeItem('refreshToken');
		localStorage.removeItem('userRoles');
	}
}
