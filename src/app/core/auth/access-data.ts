// Samplce access data
// tslint:disable-next-line:max-line-length
// {"id":1,"username":"admin","password":"demo","email":"admin@demo.com","accessToken":"access-token-0.022563452858263444","refreshToken":"access-token-0.9348573301432961","roles":["ADMIN"],"pic":"./assets/app/media/img/users/user4.png","fullname":"Mark Andre"}
export interface AccessData {
	nombres: string;
	usuario: string;
	aprobador: string;
	cuentaUsuario: string;
	correo: string;
	direccion: string;
	pic: string;
	rol: string;
	dRol: string;
	empresa: string;
	accessToken: string;
	refreshToken: string;
	roles: any;
}
