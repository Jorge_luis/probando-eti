import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ReportesService {

  constructor(
    private http: HttpClient
  ) { }

  public getReporteServicios(Tipo: number, Cliente: number, Conductor: number, Unidad: number, Placa: string, FechaInicio: string, FechaFin: string) {
	  return this.http.get(`/WS_OW/api/reportes/reporteServicio?Tipo=${Tipo}&Cliente=${Cliente}&Conductor=${Conductor}&Unidad=${Unidad}&Placa=${Placa}&FechaInicio=${FechaInicio}&FechaFin=${FechaFin}`).pipe( map( data => data ));
  }

  public getReporteServiciosSinLiquidar(Area: string, FechaInicio: string, FechaFin: string) {
	  return this.http.get(`/WS_OW/api/reportes/reporteServicioSinLiquidar?Area=${Area}&FechaInicio=${FechaInicio}&FechaFin=${FechaFin}`).pipe( map( data => data ));
  }

}
