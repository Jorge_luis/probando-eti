import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment.prod';
import { Observable } from 'rxjs';
import { PropietarioModel } from '../../content/pages/components/configuracion/propietarios/_core/model/propietario.model';

@Injectable({
  providedIn: 'root'
})
export class PropietarioService {

  constructor(
    private http: HttpClient,
  ) {  }

  public getPropietarios(Propietario: string) {
	  return this.http.get(`/WS_OW/api/propietario/propietarios?Propietario=${Propietario}`).pipe( map( data => data ));
  }

  getDatosPropietario(Propietario: number): Observable<PropietarioModel> {
		return this.http.get<PropietarioModel>(`/WS_OW/api/propietario/propietarios` + `?Propietario=${Propietario}`).pipe( map( data => data ));
  }

  public putActivarPropietario(Propietario: number, Activo: boolean, UsuarioRegistro: string) {
    return this.http.put(`/WS_OW/api/propietario/updateActivoPropietario`, {Propietario: Propietario, Activo: Activo, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }

  createPropietario(Propietario): Observable<PropietarioModel> {
		return this.http.post<PropietarioModel>(`/WS_OW/api/propietario/createPropietario`, Propietario);
  }

  updatePropietario(Propietario): Observable<PropietarioModel> {
		return this.http.post<PropietarioModel>(`/WS_OW/api/propietario/updatePropietario`, Propietario);
  }

  

}
