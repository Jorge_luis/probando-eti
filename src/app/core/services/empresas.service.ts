import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment.prod';
import { Observable } from 'rxjs';
import { EmpresaModel } from '../../content/pages/components/configuracion/empresas/_core/models/empresa.model';
import { ContactoModel } from '../../content/pages/components/configuracion/empresas/_core/models/contacto.model';
import { Areamodel } from '../../content/pages/components/configuracion/empresas/_core/models/area.model';
import { AprobadorModel } from '../../content/pages/components/configuracion/empresas/_core/models/aprobador.model';


@Injectable({
  providedIn: 'root'
})
export class EmpresasService {

  constructor(
    private http: HttpClient,
  ) {  }

  public getEmpresas(Estado: number) {
	  return this.http.get(`/WS_CL/api/empresa/empresa?Estado=${Estado}`).pipe( map( data => data ));
  }

  /* MANTENEDOR DATOS CLIENTE */
  getDatosEmpresa(Cliente: number): Observable<EmpresaModel> {
		return this.http.get<EmpresaModel>(`/WS_OW/api/empresa/datoEmpresa` + `?Cliente=${Cliente}`).pipe( map( data => data ));
  }

  createEmpresa(empresa): Observable<EmpresaModel> {
		return this.http.post<EmpresaModel>(`/WS_OW/api/empresa/createEmpresa`, empresa);
  }

  updateEmpresa(empresa): Observable<EmpresaModel> {
		return this.http.put<EmpresaModel>(`/WS_OW/api/empresa/updateEmpresa`, empresa);
  }

  public putActivarCliente(Cliente: number, Activo: boolean, UsuarioRegistro: string) {
    return this.http.put(`/WS_OW/api/empresa/updateActivoCliente`, {Cliente: Cliente, Activo: Activo, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }

  /* MANTENEDOR DATOS CONTACTO CLIENTES */
  public getContactosCliente(Cliente: number) {
	  return this.http.get(`/WS_OW/api/empresa/contactoCliente?Cliente=${Cliente}`).pipe( map( data => data ));
  }

  createContacto(empresa): Observable<EmpresaModel> {
		return this.http.post<EmpresaModel>(`/WS_OW/api/empresa/createContacto`, empresa);
  }

  updateContacto(contacto): Observable<ContactoModel> {
		return this.http.put<ContactoModel>(`/WS_OW/api/empresa/updateContacto`, contacto);
  }

  public putActivarContacto(Contacto: number, Activo: boolean, UsuarioRegistro: string) {
    return this.http.put(`/WS_OW/api/empresa/updateActivoContacto`, {Contacto: Contacto, Activo: Activo, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }

  /* MANTENEDOR DATOS AREAS CLIENTES */
  public getAreaClientes(Cliente: number) {
	  return this.http.get(`/WS_OW/api/empresa/areaCliente?Cliente=${Cliente}`).pipe( map( data => data ));
  }

  createArea(area): Observable<Areamodel> {
		return this.http.post<Areamodel>(`/WS_OW/api/empresa/createArea`, area);
  }

  updateArea(area): Observable<Areamodel> {
		return this.http.put<Areamodel>(`/WS_OW/api/empresa/updateArea`, area);
  }

  public putActivarArea(Area: number, Activo: boolean, UsuarioRegistro: string) {
    return this.http.put(`/WS_OW/api/empresa/updateActivoArea`, {Area: Area, Activo: Activo, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }



  /* MANTENEDOR DATOS APROBADORES CLIENTES */
  public getAprobadoresClientes(Area: number) {
	  return this.http.get(`/WS_OW/api/empresa/aprobadorCliente?Area=${Area}`).pipe( map( data => data ));
  }

  public putActivarAprobador(Aprobador: number, Activo: boolean, UsuarioRegistro: string) {
    return this.http.put(`/WS_OW/api/empresa/updateActivoAprobador`, {Aprobador: Aprobador, Activo: Activo, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }

  createAprobadores(aprobador): Observable<AprobadorModel> {
		return this.http.post<AprobadorModel>(`/WS_OW/api/empresa/createAprobador`, aprobador);
  }

  updateAprobadores(aprobador): Observable<AprobadorModel> {
		return this.http.put<AprobadorModel>(`/WS_OW/api/empresa/updateAprobadores`, aprobador);
  }

}
