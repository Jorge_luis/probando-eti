import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AprobacionSolicitudService {

  constructor(
    private http: HttpClient
  ) { console.log('Empezar usar servicio conformidad'); }

  // APROBACION Y RECHAZO DE SOLICITUD DE REGISTRO USUARIO CORPORATIVO
  public getServiciosEmpresa(Cliente: string, FechaInicio: string, FechaFin: string, Estado: string) {
	  return this.http.get(`/WS_CL/api/solicitud/servicioEmpresa?Cliente=${Cliente}&FechaInicio=${FechaInicio}&FechaFin=${FechaFin}&Estado=${Estado}`).pipe( map( data => data ));
  }

  public getServiciosCorporativos(Area: string, Estado: string) {
	  return this.http.get(`/WS_CL/api/solicitud/servicioCorporativo?Area=${Area}&Estado=${Estado}`).pipe( map( data => data ));
  }

  public putRechazoSolicitud(Solicitud: number, Token: string) {
    return this.http.put(`/WS_CL/api/solicitud/rechazoSolicitud`, {Solicitud: Solicitud, Token: Token})
      .pipe( map( data => data )
    );
  }

  public putAceptarSolicitud(Solicitud: number, Token: string) {
    return this.http.put(`/WS_CL/api/solicitud/confirmacionSolicitud`, {Solicitud: Solicitud, Token: Token})
      .pipe( map( data => data )
    );
  }

  rechazarUsuarios(ids: number[] = []) {
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			tasks$.push(this.putRechazoSolicitud(ids[i]['id'], ids[i]['token']));
		}
		return forkJoin(tasks$);
  }

  confirmarUsuarios(ids: number[] = []) {
		const tasks$ = [];
		const length = ids.length;
		for (let i = 0; i < length; i++) {
			tasks$.push(this.putAceptarSolicitud(ids[i]['id'], ids[i]['token']));
		}
		return forkJoin(tasks$);
  }

  // APROBACION Y RECHAZO DE SOLICITUD DE TAXI CORPORATIVO
  public getServicioAreas(Area: string) {
	  return this.http.get(`/WS_CL/api/solicitud/servicioAreas?Area=${Area}`).pipe( map( data => data ));
  }

  public putAceptarServicio(Solicitud: number, UsuarioRegistro: string, Estado: string, Token: string, Viaje: number, Observacion: string, Area: number) {
    return this.http.put(`/WS_CL/api/solicitud/confirmacionServicio`, {UsuarioRegistro: UsuarioRegistro, Solicitud: Solicitud, Estado: Estado, Token: Token, Viaje: Viaje, Observacion: Observacion, Area: Area})
      .pipe( map( data => data )
    );
  }

  public putRechazarServicio(Solicitud: number, UsuarioRegistro: string, Estado: string, Token: string, Viaje: number) {
    return this.http.put(`/WS_CL/api/solicitud/rechazarServicio`, {UsuarioRegistro: UsuarioRegistro, Solicitud: Solicitud, Estado: Estado, Token: Token, Viaje: Viaje})
      .pipe( map( data => data )
    );
  }

  // REPORTE HISTORIAL APROBACIONES
  public getHistorialAprobacion(Aprobador: string, FechaInicio: string, FechaFin: string, Estado: string) {
	  return this.http.get(`/WS_CL/api/solicitud/historialAprobacion?Aprobador=${Aprobador}&FechaInicio=${FechaInicio}&FechaFin=${FechaFin}&Estado=${Estado}`).pipe( map( data => data ));
  }

  // ASIGNACIÓN DE VIAJES
  public getViajesSolicitados() {
	  return this.http.get(`/WS_OW/api/asignacion/servicioAsignacion`).pipe( map( data => data ));
  }


}
