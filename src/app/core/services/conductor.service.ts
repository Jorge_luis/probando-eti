import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment.prod';
import { Observable } from 'rxjs';
import { ConductorModel } from '../../content/pages/components/configuracion/conductor/_core/model/conductor.model';

@Injectable({
  providedIn: 'root'
})
export class ConductorService {

  constructor(
    private http: HttpClient,
  ) {  }

  public getConductores(Conductor: string) {
	  return this.http.get(`/WS_OW/api/conductor/conductores?Conductor=${Conductor}`).pipe( map( data => data ));
  }

  getDatosEmpresa(Cliente: number): Observable<ConductorModel> {
		return this.http.get<ConductorModel>(`/WS_OW/api/conductor/conductores` + `?Conductor=${Cliente}`).pipe( map( data => data ));
  }

  public putActivarConductor(Conductor: number, Activo: boolean, UsuarioRegistro: string) {
    return this.http.put(`/WS_OW/api/conductor/updateActivoConductor`, {Conductor: Conductor, Activo: Activo, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }

  createConductor(conductor): Observable<ConductorModel> {
		return this.http.post<ConductorModel>(`/WS_OW/api/conductor/createConductor`, conductor);
  }

  updateConductor(conductor): Observable<ConductorModel> {
		return this.http.post<ConductorModel>(`/WS_OW/api/conductor/updateConductor`, conductor);
  }

}
