import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class LiquidacionService {

  constructor(
    private http: HttpClient
  ) {  }

  public getLiquidacion(Tipo: string, Cliente: number, FechaInicio: string, FechaFin: string) {
	  return this.http.get(`/WS_OW/api/liquidacion/liquidacion?Tipo=${Tipo}&Cliente=${Cliente}&FechaInicio=${FechaInicio}&FechaFin=${FechaFin}`).pipe( map( data => data ));
  }

  public getServiciosCorporativos(Cliente: string) {
	  return this.http.get(`/WS_OW/api/liquidacion/viajeCorporativo?Cliente=${Cliente}`).pipe( map( data => data ));
  }

  public getAreas(Cliente: number, Estado: number) {
	  return this.http.get(`/WS_OW/api/liquidacion/area?Estado=${Estado}&Cliente=${Cliente}`).pipe( map( data => data ));
  }

  public getCodigoLiquidacion(Tipo: string) {
	  return this.http.get(`/WS_OW/api/liquidacion/codigoLiquidacion?Tipo=${Tipo}`).pipe( map( data => data ));
  }

  public registerLiquidacionEmpresa(Cliente: string, Viajes: string, Monto: number, UsuarioRegistro: string) {
    return this.http.post(`/WS_OW/api/liquidacion/liquidacionEmpresa`, {Cliente: Cliente, Viajes: Viajes, Monto: Monto, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }


  public registerLiquidacionConductor(Conductor: string, Viajes: string, Porcentaje: number, Monto: number, UsuarioRegistro: string, Estado: string) {
    return this.http.post(`/WS_OW/api/liquidacion/liquidacionConductor`, {Conductor: Conductor, Viajes: Viajes, Porcentaje: Porcentaje , Monto: Monto, UsuarioRegistro: UsuarioRegistro, Estado: Estado})
      .pipe( map( data => data )
    );
  }

  public getDatosLiquidacion(Liquidacion: number) {
	  return this.http.get(`/WS_OW/api/liquidacion/datoLiquidacion?Liquidacion=${Liquidacion}`).pipe( map( data => data ));
  }

  public getDetalleLiquidacion(Liquidacion: number) {
	  return this.http.get(`/WS_OW/api/liquidacion/detalleLiquidacion?Liquidacion=${Liquidacion}`).pipe( map( data => data ));
  }

  public putCancelacionLiquidacion(Liquidacion: number, Tipo: number, TipoDoc: string, Serie: string, Numero: string, Usuario: string) {
    return this.http.put(`/WS_OW/api/liquidacion/cancelacionLiquidacion`, {Liquidacion: Liquidacion, Tipo: Tipo, TipoDoc: TipoDoc, Serie: Serie, Numero: Numero, UsuarioRegistro: Usuario})
      .pipe( map( data => data )
    );
  }

  public putUpdateTarifa(Viaje: number, Monto: number, Usuario: string, Codigo: number, Tipo: number) {
    return this.http.put(`/WS_OW/api/liquidacion/updateTarifa`, {Viaje: Viaje, Monto: Monto, UsuarioRegistro: Usuario, Codigo: Codigo, Tipo: Tipo})
      .pipe( map( data => data )
    );
  }

  // LIQUIDACION CONDUCTORES
  public getConductores(Estado: string) {
    return this.http.get(`/WS_OW/api/liquidacion/conductor?Estado=${Estado}`).pipe( map( data => data ));
  }

  public getServiciosConductor(Conductor: string, FechaInicio: string, FechaFin: string) {
	  return this.http.get(`/WS_OW/api/liquidacion/servicioConductor?Conductor=${Conductor}&FechaInicio=${FechaInicio}&FechaFin=${FechaFin}`).pipe( map( data => data ));
  }

  /* ASIGNACION DE SERVICIOS */
  public putAsignacionServicio(Viaje: number, Unidad: number, Tipo: number, Tiempo: number, Usuario: string, Token: string) {
    return this.http.put(`/WS_OW/api/asignacion/asignacionServicioConductor`, {Viaje: Viaje, Unidad: Unidad, Tipo: Tipo, Tiempo: Tiempo, UsuarioRegistro: Usuario, Token: Token})
      .pipe( map( data => data )
    );
  }

   /* REGISTRAR OBSERVACION SERVICIO */
   public putObservacionViaje(Viaje: number, Observacion: string, Usuario: string) {
    return this.http.put(`/WS_OW/api/liquidacion/observacionViaje`, {Viaje: Viaje, Observacion: Observacion, UsuarioRegistro: Usuario})
      .pipe( map( data => data )
    );
  }

  public putAtenderObservacionViaje(Viaje: number) {
    return this.http.put(`/WS_OW/api/liquidacion/atenderObservacionViaje`, {Viaje: Viaje})
      .pipe( map( data => data )
    );
  }

  public putAprobarLiquidacion(Liquidacion: number, Estado: string, UsuarioRegistro: string) {
    return this.http.put(`/WS_OW/api/liquidacion/aprobarLiquidacion`, {Liquidacion: Liquidacion, Estado: Estado, UsuarioRegistro: UsuarioRegistro})
      .pipe( map( data => data )
    );
  }

}
